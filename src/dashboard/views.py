
import json

from datetime import date, datetime, time
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest

from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView


from .models import Events, Snivel
from .forms import EventsForm, SnivelForm, SnivelMasterForm
from profiles.models import Profile, Team
from command.models import Unit, Department, Division
from app_manager.models import App_manager

User = get_user_model()

def home(request, *args, **kwargs):
	if request.user.is_authenticated():
			if request.user.profile.force_password_change:
				return redirect('account_change_password')
			else:
				user = get_object_or_404(User, username=request.user)
				profile = Profile.objects.get(user=request.user)
				app_list = App_manager.objects.filter(
							Q(department__in=profile.department.all())&
							Q(unit=profile.unit)
							  )
				event_list = Events.objects.filter(
							Q(team__teammember__member=user)|
							Q(poc=user)
							).distinct().order_by('start')
				snivel_list = Snivel.objects.filter(
								Q(member=user)&
								Q(end__gte=datetime.now())
								).order_by('start')
				context = {
					"profile": profile,
					"app_list": app_list,
					"event_list": event_list,
					"snivel_list": snivel_list,
				}
				return render(request, "dashboard/home.html", context)
	context = {	
	}
	return render(request, "home.html", context)

@login_required
def events_main(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	profile = Profile.objects.get(user=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	event_list = Events.objects.filter(
				Q(team__teammember__member=user)|
				Q(poc=user)
				).distinct().order_by('start')

	context = {
		"profile": profile,
		"event_list": event_list,
		
		}
	return render(request, "dashboard/events_main.html", context)

@login_required
def events_detail(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	profile = Profile.objects.get(user=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	event = Events.objects.get(pk=kwargs.get('pk'))
	
	context = {
		"profile": profile,
		"event": event,
		}
	return render(request, "dashboard/events_detail.html", context)


class EventsCreateView(PermissionRequiredMixin, CreateView):
	permission_required = 'dashboard.add_events'
	raise_exception=True
	model = Events
	template_name = "dashboard/events_form.html"
	form_class = EventsForm
	def get_initial(self):
		today = date.today()
		poc = self.request.user
		return { 'poc':poc, 'start': today, 'end': today } 
	def get_success_url(self):
		return reverse("dashboard:events_main")


class EventsUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'dashboard.change_events'
    raise_exception=True
    model = Events
    template_name = "form.html"
    form_class = EventsForm
    
    def get_success_url(self):
         return reverse("dashboard:events_main")

class EventsDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'dashboard.delete_events'
    raise_exception=True
    model = Events

    def get_success_url(self):
        return reverse("dashboard:events_main")

# Snivel

@login_required
@permission_required('profiles.add_team', raise_exception=True)
def snivel_master(request, **kwargs):
	today = date.today()
	user = get_object_or_404(User, username=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	profile = Profile.objects.get(pk=kwargs.get('pk'))
	snivel_list = Snivel.objects.filter(
								Q(member__profile=profile)&
								Q(end__gte=datetime.now())
								).order_by('start')
	form = SnivelMasterForm(request.POST or None,
								initial={'start':today,
										 'end':today,})
	
	context = {
		"profile": profile,
		"user": user,
		"snivel_list": snivel_list,
		"form": form,
		}
	return render(request, "dashboard/snivel_master.html", context)

class SnivelCreateView(LoginRequiredMixin, CreateView):
	raise_exception=True
	model = Snivel
	template_name = "form.html"
	form_class = SnivelForm
	def get_initial(self):
		today = date.today()
		return { 'start': today, 'end': today } 	
	def form_valid(self, form):
		obj = form.save(commit=False)
		obj.member = self.request.user
		return super(SnivelCreateView, self).form_valid(form)

	def get_success_url(self):
		return reverse("dashboard:home")

@login_required
@permission_required('dashboard.add_snivel', raise_exception=True)
def snivel_create_modal(request, **kwargs):
	today = date.today()
	form = SnivelMasterForm(request.POST or None,
								initial={'start':today,
										 'end':today,})
	if request.method == "POST":	
		if form.is_valid():
			member = form.cleaned_data['member']
			start = form.cleaned_data['start']
			end = form.cleaned_data['end']
			description = form.cleaned_data['description']
			
			instance = Snivel()
			instance.member = member
			instance.start = start
			instance.end = end
			instance.description = description
			instance.save()
			
			return HttpResponse('Success')			
		if form.errors:
			print form.errors
			json_data = json.dumps(form.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"form": form,
	}
	return render(request, "form.html", context)


class SnivelUpdateView(PermissionRequiredMixin, UpdateView):
	permission_required = 'dashboard.change_snivel'
	raise_exception=True
	model = Snivel
	template_name = "form.html"
	form_class = SnivelForm
	


	def form_valid(self, form):
		obj = form.save(commit=False)
		obj.member = self.request.user
		return super(SnivelMasterUpdateView, self).form_valid(form)	

	def get_success_url(self):
		 return reverse("dashboard:home")

class SnivelMasterUpdateView(PermissionRequiredMixin, UpdateView):
	permission_required = 'dashboard.change_snivel'
	raise_exception=True
	model = Snivel
	template_name = "dashboard/snivel_form.html"
	form_class = SnivelForm

	def get_object(self):
		return Snivel.objects.get(pk=self.kwargs.get('pk_2'))

	def get_context_data(self, **kwargs):
		context = super(SnivelMasterUpdateView, self).get_context_data(**kwargs)
		self.profile = Profile.objects.get(pk=self.kwargs['pk'])
		context['profile'] = self.profile
		return context

	def get_success_url(self):
		self.profile = Profile.objects.get(pk=self.kwargs['pk'])		
		return reverse("profiles:snivel_master", kwargs={'pk':self.profile.pk})

class SnivelMasterDeleteView(PermissionRequiredMixin, DeleteView):
	permission_required = 'dashboard.delete_snivel'
	raise_exception=True
	model = Snivel
	template_name = "dashboard/snivel_master_confirm_delete.html"

	def get_object(self):
		return Snivel.objects.get(pk=self.kwargs.get('pk_2'))

	def get_context_data(self, **kwargs):
		context = super(SnivelMasterDeleteView, self).get_context_data(**kwargs)
		self.profile = Profile.objects.get(pk=self.kwargs['pk'])
		context['profile'] = self.profile
		return context

	def get_success_url(self):
		self.profile = Profile.objects.get(pk=self.kwargs['pk'])		
		return reverse("profiles:snivel_master", kwargs={'pk':self.profile.pk})


class SnivelDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'dashboard.delete_snivel'
    raise_exception=True
    model = Snivel

    def get_success_url(self):
        return reverse("dashboard:home")

		