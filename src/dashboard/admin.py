from django.contrib import admin

from .models import Events, Snivel

    
class EventsAdmin(admin.ModelAdmin):
        list_display = [
        "__unicode__",
        "title",
        "poc",
]

class SnivelAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    ]

    
admin.site.register(Events, EventsAdmin)
admin.site.register(Snivel, SnivelAdmin)
