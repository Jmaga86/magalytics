from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import TemplateView

from .views import (
                    home,
                    events_main,
                    events_detail,
                    EventsCreateView,
                    EventsUpdateView,
                    EventsDeleteView,
                    SnivelCreateView,
                    SnivelUpdateView,
                    SnivelDeleteView,
                    SnivelMasterUpdateView,
                    SnivelMasterDeleteView

                    )


urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^events_manager/$', events_main, name='events_main'),
    url(r'^events_manager/(?P<pk>\d+)/$', events_detail, name='events_detail'),
    url(r'^events_manager/create/$', EventsCreateView.as_view(), name='events_create'),    
    url(r'^events_manager/(?P<pk>\d+)/update/$', EventsUpdateView.as_view(), name='events_update'),
    url(r'^events_manager/(?P<pk>\d+)/delete/$', EventsDeleteView.as_view(), name='events_delete'),
    url(r'^snivel/create/$', SnivelCreateView.as_view(), name='snivel_create'),
    url(r'^snivel/(?P<pk>\d+)/update/$', SnivelUpdateView.as_view(), name='snivel_update'),
    url(r'^snivel/(?P<pk>\d+)/master/(?P<pk_2>\d+)/update/$', SnivelMasterUpdateView.as_view(), name='snivel_master_update'),    
    url(r'^snivel/(?P<pk>\d+)/delete/$', SnivelDeleteView.as_view(), name='snivel_delete'),
    url(r'^snivel/(?P<pk>\d+)/master/(?P<pk_2>\d+)/delete/$', SnivelMasterDeleteView.as_view(), name='snivel_master_delete'),    
   
]
