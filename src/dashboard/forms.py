from django import forms
from django.db import models
from django.forms.widgets import SelectDateWidget
from .models import (
                Events,
                Snivel,
                )

class EventsForm(forms.ModelForm):
    class Meta:
        model = Events
        fields = [
            'title',
            'poc',
            'start',
            'end',
            'team',
            'description',
        ]
        widgets = { 'start': SelectDateWidget(years=range(2010, 2030)),
                'end': SelectDateWidget(years=range(2010, 2030)),
                   }

class SnivelForm(forms.ModelForm):
    class Meta:
        model = Snivel
        fields = [
            'start',
            'end',
            'description',
        ]
        widgets = { 'start': SelectDateWidget(years=range(2010, 2030)),
                'end': SelectDateWidget(years=range(2010, 2030)),
                   }

class SnivelMasterForm(forms.ModelForm):
    class Meta:
        model = Snivel
        fields = [
            'member',    
            'start',
            'end',
            'description',
        ]
        widgets = { 'start': SelectDateWidget(years=range(2010, 2030)),
                'end': SelectDateWidget(years=range(2010, 2030)),
                   }