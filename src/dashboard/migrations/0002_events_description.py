# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-19 17:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='events',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
    ]
