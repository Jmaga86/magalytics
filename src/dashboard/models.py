from __future__ import unicode_literals

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models

from profiles.models import Team

User = settings.AUTH_USER_MODEL

class Events(models.Model):
    title = models.CharField(max_length=140)
    poc = models.ForeignKey(User)
    start = models.DateField()
    end = models.DateField()
    team = models.ForeignKey(Team, null=True, blank=True)
    description = models.TextField(max_length=None, null=True, blank=True)
    
    def __unicode__(self):
        return self.title
    
class Snivel(models.Model):
    member = models.ForeignKey(User)
    start = models.DateField()
    end = models.DateField()
    description = models.TextField(max_length=None, null=True, blank=True)
    
    def __unicode__(self):
        return self.member.username