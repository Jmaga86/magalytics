from django import forms
from django.db import models
from django.forms.widgets import SelectDateWidget
from .models import (
                Ship_class,
                Class_variant,
                Ship,
                Department,
                Gen_Cert,
                Local_Cert,
                Discrepancy,
                Discrepancy_image,
                Assess_name,
                Space,
                Equipment,
                Question,
                Answer,
                Assessment,
                System,
)

class Ship_classForm(forms.ModelForm):
    class Meta:
        model = Ship_class
        fields = [
            'name',
        ]

class Class_variantForm(forms.ModelForm):
    class Meta:
        model = Class_variant
        fields = [
            'variant_name',
            'ship_class',
        ]        

class ShipForm(forms.ModelForm):
    class Meta:
        model = Ship
        fields = [
            'ship_name',
            'ship_ID',
            'ship_class',
            'class_variant',
            'uic',
            'hull_number',
            'crew',
            'image',
        ]

class SystemForm(forms.ModelForm):
    class Meta:
        model = System
        fields = [
            'name',
            'ship_class',
            'variant',
            'ship',
        ]
        
class Gen_CertForm(forms.ModelForm):
    class Meta:
        model = Gen_Cert
        fields = [
            'certname',
            'ships',
            'instruction',
            'perodicity',
        ]
        
class Local_CertForm(forms.ModelForm):
    class Meta:
        model = Local_Cert
        fields = [
                'name',
                'cert_date',
                'expiration_date',
                'next_scheduled',
                'poc',
                'document',
                'pre_com',
        ]
        widgets = { 'cert_date': SelectDateWidget(years=range(2010, 2030)),
                        'expiration_date': SelectDateWidget(years=range(2010, 2030)),
                           }
class DiscrepancyForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DiscrepancyForm, self).__init__(*args, **kwargs)
        self.fields['department'].empty_label=None
        self.fields['casrep'].empty_label=None
    class Meta:
        model = Discrepancy
        fields = [
            'user',
            'ships',
            'system',
            'department',
            'description',
            'discover_date',
            'repair_schedule',
            'casrep',
            'sat',
            'sto',
            'jsn',
            'active',

        ]
        widgets = { 'discover_date': SelectDateWidget(years=range(2010, 2030)),
                   'description': forms.Textarea(attrs={'rows':10, 'cols':50})
                           }


class Discrepancy_imageForm(forms.ModelForm):
	class Meta:
		model = Discrepancy_image
		fields = [
			'discrepancy',
			'image',
		]


class Assess_nameForm(forms.ModelForm):
    class Meta:
        model = Assess_name
        fields = [
            'name',
        ]

class SpaceForm(forms.ModelForm):
    class Meta:
        model = Space
        fields = [
            'ship',
            'name',
            'assess_name',
        ]

class EquipmentForm(forms.ModelForm):
    class Meta:
        model = Equipment
        fields = [
            'ship',
            'space',
            'name',
            'assess_name'
        ]
        
class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = [
            'question',
            'answers',
            'question_order',
        ]
    
        
class AssessmentForm(forms.Form):
    # ship_name = forms.ModelChoiceField(queryset=Ship.objects.all(), empty_label=None)
    question_id = forms.IntegerField(widget = forms.HiddenInput())
    answer_id = forms.IntegerField()        