import csv
from django.utils.encoding import smart_str
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseBadRequest
from django.shortcuts import render, render_to_response
from .models import Discrepancy, Ship

def export_csv_hits(query):
	queryset = query
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename=export.csv'
	writer = csv.writer(response, csv.excel)
	response.write(u'\ufeff'.encode('utf8'))
	writer.writerow([
		smart_str(u"Ship Name"),
		smart_str(u"System"),
		smart_str(u"Department"),
		smart_str(u"JSN"),
		smart_str(u"Description"),
		smart_str(u"Date Discovered"),
		smart_str(u"SAT"),
		smart_str(u"STO"),
		smart_str(u"CASREP Category"),		
		smart_str(u"Repair Schedule"),
	])
	for obj in queryset:
		writer.writerow([
			smart_str(obj.ships),
			smart_str(obj.system),
			smart_str(obj.department),
			smart_str(obj.jsn),
			smart_str(obj.description),
			smart_str(obj.discover_date),
			smart_str(obj.sat),
			smart_str(obj.sto),
			smart_str(obj.casrep),			
			smart_str(obj.repair_schedule), 
		])
	return response

def export_csv_issues(query):
	queryset = query
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename=export.csv'
	writer = csv.writer(response, csv.excel)
	response.write(u'\ufeff'.encode('utf8'))
	writer.writerow([
		smart_str(u"Ship Name"),
		smart_str(u"System"),
		smart_str(u"JSN"),
		smart_str(u"Description"),
		smart_str(u"Date Discovered"),
		smart_str(u"CASREP Category"),		
	])
	for obj in queryset:
		writer.writerow([
			smart_str(obj.ships),
			smart_str(obj.system),
			smart_str(obj.jsn),
			smart_str(obj.description),
			smart_str(obj.discover_date),
			smart_str(obj.casrep),
	])
	return response

def export_csv_certs(query):
	queryset = query
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename=export.csv'
	writer = csv.writer(response, csv.excel)
	response.write(u'\ufeff'.encode('utf8'))
	writer.writerow([
		smart_str(u"Ship Name"),
		smart_str(u"Cert Name"),
		smart_str(u"Cert Date"),
		smart_str(u"Expiration Date"),
		smart_str(u"Next Scheduled"),
		smart_str(u"POC"),
	])
	for obj in queryset:
		writer.writerow([
			smart_str(obj.ship),
			smart_str(obj.name),
			smart_str(obj.cert_date),
			smart_str(obj.expiration_date),
			smart_str(obj.next_scheduled),
			smart_str(obj.poc),
		])
	return response