from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from .views import (
                ship_main,
                ShipDetailView,
                ShipUpdateView,
                Local_CertCreateView,
                Local_CertUpdateView,
                Loc_CertListView,
                HitDetailView,
                HitDeleteView,
                HitCreateView,
                HitUpdateView,
                HitListView,
                HitImageCreateView,
                HitImageUpdateView,
                HitImageDeleteView,
                assess_list_main,
                SpaceListView,
                SpaceCreateView,
                SpaceUpdateView,
                SpaceDeleteView,
                EquipmentListView,
                EquipmentCreateView,
                EquipmentUpdateView,
                EquipmentDeleteView,
                question,
                discrepancy_modal,
                QuestionCreateView,
                QuestionUpdateView,
                QuestionDeleteView,
                QuestionListView,
                SystemListView,
                SystemCreateView,
                issue_tracker,
                SystemUpdateView,
                SystemDeleteView,
                )

from .functions import (
    export_csv_hits,    
)

urlpatterns = [
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/$', ship_main, name='ship_main'),
    
    #Ship URls
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/$',
        ShipDetailView.as_view(), name='ship_detail'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/update/',
        ShipUpdateView.as_view(), name='ship_update'),

    #Class Desk URLs
    url(r'^(?P<ship_main_slug>[-\w]+)/class_tracker/$',
        issue_tracker, name='issue_tracker'),
    url(r'^(?P<ship_main_slug>[-\w]+)/class_tracker/systems/$', SystemListView, name='system_list'),
    url(r'^(?P<ship_main_slug>[-\w]+)/class_tracker/systems/create/$', SystemCreateView.as_view(), name='system_create'),
    url(r'^(?P<ship_main_slug>[-\w]+)/class_tracker/systems/(?P<pk>\d+)/update/$', SystemUpdateView.as_view(), name='system_update'),
    url(r'^(?P<ship_main_slug>[-\w]+)/class_tracker/systems/(?P<pk>\d+)/delete/$', SystemDeleteView.as_view(), name='system_delete'),

    #Cert URLs
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/certs/(?P<pk>\d+)/update/$',
        Local_CertUpdateView.as_view(), name='loc_cert_update'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/certs/create/$',
        Local_CertCreateView.as_view(), name='loc_cert_create'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/certs/$',
        Loc_CertListView, name='certs'),

    
    #Discrepancy URLs    
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/hits/create/$',
        HitCreateView.as_view(), name='hit_create'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/hits/$',
        HitListView, name='hits'),   
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/hits/(?P<pk>\d+)/$',
        HitDetailView, name='hit_detail'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/hits/(?P<pk>\d+)/images/$',
        HitImageCreateView.as_view(), name='hit_image'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/hits/(?P<pk>\d+)/images/(?P<pk_1>\d+)/update/$',
        HitImageUpdateView.as_view(), name='hit_image_update'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/hits/(?P<pk>\d+)/images/(?P<pk_1>\d+)/delete/$',
        HitImageDeleteView.as_view(), name='hit_image_delete'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/hits/(?P<pk>\d+)/delete/$',
        HitDeleteView.as_view(), name='hit_delete'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/hits/(?P<pk>\d+)/update/$',
        HitUpdateView.as_view(), name='hit_update'),
    
    #Assessment Urls
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/$',
        assess_list_main, name='assess_list_main'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/create/$',
        SpaceCreateView.as_view(), name='space_create'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk>\d+)/update/$',
        SpaceUpdateView.as_view(), name='space_update'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk>\d+)/delete/$',
        SpaceDeleteView.as_view(), name='space_delete'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/$',
        SpaceListView.as_view(), name='space_list'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/create/$',
        EquipmentCreateView.as_view(), name='equipment_create'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/(?P<pk>\d+)/update/$',
        EquipmentUpdateView.as_view(), name='equipment_update'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/(?P<pk>\d+)/delete/$',
        EquipmentDeleteView.as_view(), name='equipment_delete'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/$',
        EquipmentListView.as_view(), name='equipment_list'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/(?P<pk>\d+)/$',
        question , name='assessment'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/(?P<pk_2>\d+)/questions/$',
        QuestionListView.as_view(), name='question_list'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/(?P<pk_2>\d+)/questions/create/$',
        QuestionCreateView.as_view(), name='question_create'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/(?P<pk_2>\d+)/questions/(?P<pk>\d+)/update/$',
        QuestionUpdateView.as_view(), name='question_update'),
    url(r'^(?P<ship_main_slug>[-\w]+)/ships/(?P<ship_slug>[-\w]+)/assessments/(?P<assess_slug>[-\w]+)/space/(?P<pk_1>\d+)/equipment/(?P<pk_2>\d+)/questions/(?P<pk>\d+)/delete/$',
        QuestionDeleteView.as_view(), name='question_delete'),

]

if settings.DEBUG:
    urlpatterns+= static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)