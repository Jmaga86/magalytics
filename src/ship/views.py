import json
import csv
from django.utils.encoding import smart_str


from datetime import date, datetime, time
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.conf import Settings
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseBadRequest
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.shortcuts import render, render_to_response, get_object_or_404
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.utils.decorators import method_decorator
from .functions import export_csv_hits, export_csv_issues, export_csv_certs

from itertools import chain

from command.models import Unit
from profiles.models import Profile
from .models import (
				Ship_class,
				Class_variant,
				Ship,
				Department,
				Gen_Cert,
				Local_Cert,
				Discrepancy,
				Discrepancy_image,
				Assess_name,
				Space,
				Equipment,
				Question,
				Answer,
				Assessment,
				ship_main,
				System,
)

from .forms import (
				ShipForm,
				Gen_CertForm,
				Local_CertForm,
				DiscrepancyForm,
				Discrepancy_imageForm,
				Assess_nameForm,
				SpaceForm,
				EquipmentForm,
				QuestionForm,
				AssessmentForm,
				SystemForm,
				)

User = get_user_model()

class MultipleObjectMixin(object):
     model = None
     def get_object(self, *args, **kwargs):
          system_slug = self.kwargs.get("system_slug")
          ModelClass = self.model
          if system_slug is not None:
               try:
                    obj = get_object_or_404(ModelClass, system_slug=system_slug)
               except self.model.MultipleObjectsReturned:
                    obj = ModelClass.objects.filter(certslug=certslug).order_by("-pk").first()
          else:
               obj = super(MultipleObjectMixin, self).get_object(*args, **kwargs)
          
          return obj

#Ship Models

@login_required
def ship_main(request, **kwargs):
    user = get_object_or_404(User, username=request.user)
    profile = Profile.objects.get(user=request.user)
    unit = Unit.objects.get(name=request.user.profile.unit.name)
    ship_list = Ship.objects.filter(uic__in=unit.assets.all()).order_by('hull_number')
    main_slug = kwargs.get('ship_main_slug')
    context = {
        "ship_list": ship_list,
        "main_slug": main_slug,
        }
    return render(request, "ship/ship_main.html", context)

class ShipDetailView(DetailView):    
    model = Ship
    slug_field = 'ship_main_slug'
    def get_object(self):
        return Ship.objects.get(ship_slug=self.kwargs.get('ship_slug'))

    def get_context_data(self, **kwargs):
         context = super(ShipDetailView, self).get_context_data(**kwargs)
         context['main_slug'] = self.kwargs.get('ship_main_slug')
         return context

class ShipCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'ship.add_ship'
    raise_exception=True
    model = Ship
    template_name = "form.html"
    form_class = ShipForm
    def get_success_url(self):
        return reverse("settings")


class ShipUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'ship.change_ship'
    raise_exception=True
    model = Ship
    template_name = "form.html"
    form_class = ShipForm
    def get_object(self):
        return Ship.objects.get(ship_slug=self.kwargs.get('ship_slug'))

    def upload_file(request):
         if request.method == 'POST':
             form = ShipForm(request.POST or None, request.FILES or None)
             if form.is_valid():
                 handle_uploaded_file(request.FILES['file'])
         else:
             form = UploadFileForm()
    
    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')      
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        return reverse("ship:ship_detail", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug}) 

class SystemCreateView(PermissionRequiredMixin, CreateView):
	permission_required = 'ship.add_system'
	raise_exception=True
	model = System
	template_name = "form.html"
	form_class = SystemForm
	
	def get_success_url(self):
		main_slug = self.kwargs.get('ship_main_slug')      
		return reverse("ship:system_list", kwargs={'ship_main_slug':main_slug})

class SystemUpdateView(PermissionRequiredMixin, UpdateView):
	permission_required = 'ship.change_system'
	raise_exception=True
	model = System
	form_class = SystemForm
	template_name = "form.html"
	
	def get_success_url(self):
		main_slug = self.kwargs.get('ship_main_slug')      
		return reverse("ship:system_list", kwargs={'ship_main_slug':main_slug})

class SystemDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'ship.delete_system'
    raise_exception=True
    model = System
    def get_context_data(self, **kwargs):
        context = super(SystemDeleteView, self).get_context_data(**kwargs)
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        return context

    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug') 
        return reverse("ship:system_list", kwargs={'ship_main_slug':main_slug})

@login_required
@permission_required('ship.add_system', raise_exception=True)
def SystemListView(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	profile = Profile.objects.get(user=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	ship_list = Ship.objects.filter(uic__in=unit.assets.all()).order_by('-hull_number')
	ship_class = Ship_class.objects.filter(ship__in=ship_list).distinct()
	variants = Class_variant.objects.filter(ship_class__in=ship_class)
	systems = System.objects.filter(ship_class__in=ship_class)
	main_slug = kwargs.get('ship_main_slug')		
	context = {
		"ship_list": ship_list,
		"main_slug": main_slug,
		"ship_class": ship_class,
		"variants": variants,
		"systems": systems,
		}
	return render(request, "ship/system_list.html", context)
	
	

@login_required
@permission_required('ship.add_system', raise_exception=True)
def issue_tracker(request, **kwargs):
	today = date.today()
	user = get_object_or_404(User, username=request.user)
	role = request.user.userrole
	profile = Profile.objects.get(user=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	ship_list = Ship.objects.filter(uic__in=unit.assets.all()).order_by('-hull_number')
	ship_class = Ship_class.objects.filter(ship__in=ship_list).distinct()
	variant_list = Class_variant.objects.filter(ship_class__in=ship_class)
	system_list = System.objects.filter(ship_class__in=ship_class)
	discrepancy_list = Discrepancy.objects.order_by('casrep').values('casrep').distinct()
	object_list = []
	staff = ('Staff')
	typedesk = ('TypeDesk')
	system = request.GET.get("system", "")
	ship = request.GET.get("ship", "")
	ship_variant = request.GET.get("ship_variant", "")
	cat = request.GET.get("cat", "")
	active = request.GET.get("active")
	if system or ship or ship_variant or cat or active:	
		object_list = Discrepancy.objects.filter(
								Q(user__userrole__role__icontains=typedesk)&
								Q(system__icontains=system)&
								Q(ships__ship_name__icontains=ship)&
								Q(ships__class_variant__variant_name__icontains=ship_variant)&
								Q(casrep__icontains=cat)&
								Q(active=active)|
								Q(user__userrole__role__icontains=staff)&
								Q(system__icontains=system)&
								Q(ships__ship_name__icontains=ship)&
								Q(ships__class_variant__variant_name__icontains=ship_variant)&
								Q(casrep__icontains=cat)&
								Q(active=active)								
								)

	else:
		object_list = Discrepancy.objects.filter(
								Q(user__userrole__role__icontains=typedesk)&
								Q(ships__in=ship_list)|
								Q(user__userrole__role__icontains=staff)&
								Q(ships__in=ship_list)
								)
	if request.method == 'POST':
		return export_csv_issues(object_list)	
	main_slug = kwargs.get('ship_main_slug')
	form = DiscrepancyForm(request.POST or None,
								initial={'discover_date':today })		
	context = {
		"ship_list": ship_list,
		"main_slug": main_slug,
		"ship_class": ship_class,
		"variant_list": variant_list,
		"object_list": object_list,
		"system_list": system_list,
		"discrepancy_list": discrepancy_list,
		"form": form,
		}
	return render(request, "ship/tracker_main.html", context)


@login_required
@permission_required('ship.add_discrepancy', raise_exception=True)
def issue_create_modal(request, **kwargs):
	today = date.today()
	form = DiscrepancyForm(request.POST or None,
								initial={'discover_date':today })
	if request.method == "POST":	
		if form.is_valid():
			system = form.cleaned_data['system']
			
			instance = Discrepancy()
			instance.system = system
			instance.save()
			
			return HttpResponse('Success')			
		if form.errors:
			print form.errors
			json_data = json.dumps(form.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"form": form,
	}
	return render(request, "form.html", context)


#Certification Models

class Gen_CertCreateView(PermissionRequiredMixin, CreateView):
     permission_required = 'ship.add_gen_cert'
     raise_exception=True
     model = Gen_Cert
     template_name = "form.html"
     form_class = Gen_CertForm
     
     def get_success_url(self):
          return reverse("gen_cert")

class Gen_CertListView(ListView):
     model = Gen_Cert

class Gen_CertUpdateView(PermissionRequiredMixin, UpdateView):
     permission_required = 'ship.change_gen_cert'
     raise_exception=True
     model = Gen_Cert
     template_name = "form.html"
     form_class = Gen_CertForm

     def get_success_url(self):
          return reverse( "gen_cert")

class Local_CertCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'ship.add_local_cert'
    raise_exception=True
    model = Local_Cert
    template_name = "form.html"
    form_class = Local_CertForm
    
    def upload_file(request):
         if request.method == 'POST':
             form = Local_CertForm(request.POST or None, request.FILES or None)
             if form.is_valid():
                 handle_uploaded_file(request.FILES['file'])
         else:
             form = UploadFileForm()
    
    def get_queryset(self):
         self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
         return Local_Cert.objects.filter(ship=self.ship)
    
    def get_initial(self):
         self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
         return { 'ship': self.ship}
    
    def form_valid(self, form):
         self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
         ship = self.ship
         form.instance.ship = ship
         return super(Local_CertCreateView, self).form_valid(form)
        
    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')      
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        return reverse("ship:certs", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug})

class Local_CertUpdateView(PermissionRequiredMixin, UpdateView):
     permission_required = 'ship.change_local_cert'
     raise_exception=True
     model = Local_Cert
     template_name = "form.html"
     form_class = Local_CertForm
     def upload_file(request):
          if request.method == 'POST':
              form = CertForm(request.POST or None, request.FILES or None)
              if form.is_valid():
                  handle_uploaded_file(request.FILES['file'])
          else:
              form = UploadFileForm()
     
     def get_success_url(self):
          main_slug = self.kwargs.get('ship_main_slug')      
          self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
          return reverse("ship:certs", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug})

     


@login_required
def Loc_CertListView(request, **kwargs):
	ship = Ship.objects.get(ship_slug=kwargs.get('ship_slug'))
	object_list  = Local_Cert.objects.filter(ship=ship)
	if request.method == 'POST':
		return export_csv_certs(object_list)
	main_slug = kwargs.get('ship_main_slug')		
	context = {
		"ship": ship,
		"main_slug": main_slug,
		"object_list": object_list,
		}
	return render(request, "ship/local_cert_list.html", context)


#Discrepancy Views

@login_required
def HitDetailView(request, pk=None, **kwargs):
	main_slug = kwargs.get('ship_main_slug')
	ship = Ship.objects.get(ship_slug=kwargs['ship_slug'])
	obj = get_object_or_404(Discrepancy, pk=pk)
	image_list = Discrepancy_image.objects.filter(discrepancy=obj)

	context = {
		"ship": ship,
		"main_slug": main_slug,
		"obj": obj,
		"image_list": image_list,
		}
	return render(request, "ship/hit_detail.html", context)

class HitDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'ship.delete_discrepancy'
    raise_exception=True
    model = Discrepancy
    def get_context_data(self, **kwargs):
        context = super(HitDeleteView, self).get_context_data(**kwargs)
        context['ship'] = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        return context

    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug') 
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        return reverse("ship:hits", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug})

class HitCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'ship.add_discrepancy'
    raise_exception=True
    model = Discrepancy
    template_name = "ship/hit_form.html"
    form_class = DiscrepancyForm        
    def get_initial(self):
        today = date.today()
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        return { 'ships': self.ship, 'discover_date': today }
    def get_context_data(self, **kwargs):
        context = super(HitCreateView, self).get_context_data(**kwargs)
        context['ship'] = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        return context	
    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        return reverse("ship:hits", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug})

class HitUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'ship.change_discrepancy'
    raise_exception=True
    model = Discrepancy
    template_name = "ship/hit_form.html"
    form_class = DiscrepancyForm
    def get_context_data(self, **kwargs):
        context = super(HitUpdateView, self).get_context_data(**kwargs)
        context['ship'] = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        return context    
    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        return reverse("ship:hits", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug})


@login_required
def HitListView(request, **kwargs):
	ship = Ship.objects.get(ship_slug=kwargs.get('ship_slug'))
	object_list = []
	staff = ('Staff')
	regular = ('Regular')
	query = request.GET.get("q")
	active = request.GET.get("active")
	if query:
		object_list  = Discrepancy.objects.filter(
								Q(user__userrole__role__icontains=staff)&
								Q(ships=ship)&	
								Q(system__icontains=query)|
								Q(description__icontains=query)|
								Q(casrep__icontains=query)|
								Q(user__userrole__role__icontains=regular)&
								Q(ships=ship)&
								Q(system__icontains=query)|
								Q(description__icontains=query)|
								Q(casrep__icontains=query)															
								)
	elif active:
		object_list  = Discrepancy.objects.filter(
								Q(ships=ship)&
								Q(active=active)
								)
	else:
		object_list = Discrepancy.objects.active().filter(
								Q(user__userrole__role__icontains=staff)&
								Q(ships=ship)|
								Q(user__userrole__role__icontains=regular)&
								Q(ships=ship)
								)
	if request.method == 'POST':
		return export_csv_hits(object_list)
	main_slug = kwargs.get('ship_main_slug')		
	context = {
		"ship": ship,
		"main_slug": main_slug,
		"object_list": object_list,
		}
	return render(request, "ship/hit_list.html", context)

class HitImageCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'ship.add_discrepancy'
    raise_exception=True
    model = Discrepancy_image
    template_name = "ship/hit_image_form.html"
    form_class = Discrepancy_imageForm
    def get_initial(self):
        self.discrepancy = Discrepancy.objects.get(pk=self.kwargs['pk'])
        return { 'discrepancy': self.discrepancy }
    def get_context_data(self, **kwargs):
        context = super(HitImageCreateView, self).get_context_data(**kwargs)
        context['ship'] = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        context['discrepancy'] = Discrepancy.objects.get(pk=self.kwargs['pk'])
        return context
    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.discrepancy = Discrepancy.objects.get(pk=self.kwargs['pk'])
        return reverse("ship:hit_image", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk':self.discrepancy.pk,})

class HitImageDeleteView(PermissionRequiredMixin, DeleteView):
	permission_required = 'ship.delete_discrepancy'
	raise_exception=True
	template_name = "ship/discrepancy_image_confirm_delete.html"
	model = Discrepancy_image
	
	def get_object(self):
		return Discrepancy_image.objects.get(pk=self.kwargs.get('pk_1'))
	def get_context_data(self, **kwargs):
		context = super(HitImageDeleteView, self).get_context_data(**kwargs)
		context['ship'] = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		context['main_slug'] = self.kwargs.get('ship_main_slug')
		context['discrepancy'] = Discrepancy.objects.get(pk=self.kwargs['pk'])
		return context

	def get_success_url(self):
		main_slug = self.kwargs.get('ship_main_slug') 
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.discrepancy = Discrepancy.objects.get(pk=self.kwargs['pk'])
		return reverse("ship:hit_detail", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk':self.discrepancy.pk,})
	
class HitImageUpdateView(PermissionRequiredMixin, UpdateView):
	permission_required = 'ship.delete_discrepancy'
	raise_exception=True
	model = Discrepancy_image
	template_name = "form.html"
	form_class = Discrepancy_imageForm

	def get_object(self):
		return Discrepancy_image.objects.get(pk=self.kwargs.get('pk_1'))	

	def get_context_data(self, **kwargs):
		context = super(HitImageUpdateView, self).get_context_data(**kwargs)
		context['ship'] = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		context['main_slug'] = self.kwargs.get('ship_main_slug')
		context['discrepancy'] = Discrepancy.objects.get(pk=self.kwargs['pk'])
		return context

	def get_success_url(self):
		main_slug = self.kwargs.get('ship_main_slug') 
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.discrepancy = Discrepancy.objects.get(pk=self.kwargs['pk'])
		return reverse("ship:hit_detail", kwargs={'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk':self.discrepancy.pk,})
#Assessment Views

@login_required
def assess_list_main(request, **kwargs):
    ship = Ship.objects.get(ship_slug=kwargs.get('ship_slug'))
    ass_list = Assessment.objects.filter(ship=ship).order_by('-timestamp')[:100]
    object_list = Assess_name.objects.all()
    main_slug = kwargs.get('ship_main_slug')
    context = {
        "object_list": object_list,
        "ship": ship,
        "ass_set": ass_list,
        "main_slug": main_slug,
        }
    return render(request, "ship/assess_list_main.html", context)

class Assess_nameListView(ListView):
	model = Assess_name
	
class Assess_nameCreateView(PermissionRequiredMixin, CreateView):
	permission_required = 'ship.add_Assess_name'
	raise_exception=True
	model = Assess_name
	template_name = "form.html"
	form_class = Assess_nameForm
	
	def get_success_url(self):
		return ("/settings/assessments")

class Assess_nameUpdateView(PermissionRequiredMixin, UpdateView):
     permission_required = 'ship.change_Assess_name'
     raise_exception=True
     model = Assess_name
     template_name = "form.html"
     form_class = Assess_nameForm

     def get_success_url(self):
          return reverse( "gen_assess")
    
class SpaceListView(ListView):
    model = Space
    def get_queryset(self):
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        return Space.objects.filter(ship=self.ship).filter(assess_name=self.assess_name)
    
    def get_context_data(self, **kwargs):
        context = super(SpaceListView, self).get_context_data(**kwargs)
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        context['ship'] = self.ship
        context['assess_name'] = self.assess_name
        return context

class SpaceCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'ship.add_space'
    raise_exception=True
    model = Space
    template_name = "form.html"
    form_class = SpaceForm

    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')      
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        return reverse("ship:space_list", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug})

class SpaceUpdateView(PermissionRequiredMixin ,UpdateView):
    permission_required = 'ship.change_ship'
    raise_exception=True
    model = Space
    template_name = "form.html"
    form_class = SpaceForm

    def get_queryset(self):
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        return Space.objects.filter(ship=self.ship).filter(assess_name=self.assess_name)
    
    
    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')      
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        return reverse("ship:space_list", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug})

class SpaceDeleteView(PermissionRequiredMixin ,DeleteView):
    permission_required = 'ship.delete_ship'
    raise_exception=True	
    template_name = "ship/space_confirm_delete.html"
    model = Space
    
    def get_queryset(self):
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        return Space.objects.filter(ship=self.ship).filter(assess_name=self.assess_name)

    def get_context_data(self, **kwargs):
        context = super(SpaceDeleteView, self).get_context_data(**kwargs)
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        context['ship'] = self.ship
        context['assess_name'] = self.assess_name
        return context    

    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')      
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        return reverse("ship:space_list", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug})

class EquipmentListView(ListView):
    model = Equipment
    template_name = "ship/equipment_list.html"
    def get_queryset(self):
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        self.space = Space.objects.get(pk=self.kwargs['pk_1'])
        return Equipment.objects.filter(space=self.space)
    
    def get_context_data(self, **kwargs):
        context = super(EquipmentListView, self).get_context_data(**kwargs)
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        context['ship'] = self.ship
        context['assess_name'] = self.assess_name
        context['space'] = self.space
        return context
    
class EquipmentCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'ship.add_equipment'
    raise_exception=True
    model = Equipment
    template_name = "form.html"
    form_class = EquipmentForm
    
    def get_initial(self):
        self.space = Space.objects.get(pk=self.kwargs['pk_1'])
        return { 'space':self.space }
    
    def form_valid(self, form):
        self.space = Space.objects.get(pk=self.kwargs['pk_1'])
        form.instance.space = self.space
        return super(EquipmentCreateView, self).form_valid(form)	
    
    def get_queryset(self):
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        self.space = Space.objects.get(pk=self.kwargs['pk_1'])
        return Equipment.objects.filter(space=self.space)
    
    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')      
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        self.space = Space.objects.get(pk=self.kwargs['pk_1'])
        return reverse("ship:equipment_list", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk_1':self.space.pk})

class EquipmentUpdateView(PermissionRequiredMixin ,UpdateView):
    permission_required = 'ship.change_equipment'
    raise_exception=True
    model = Equipment
    template_name = "form.html"
    form_class = EquipmentForm
    
    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')      
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        self.space = Space.objects.get(pk=self.kwargs['pk_1'])
        return reverse("ship:equipment_list", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk_1':self.space.pk})

class EquipmentDeleteView(PermissionRequiredMixin ,DeleteView):
    permission_required = 'ship.delete_equipment'
    raise_exception=True	
    template_name = "ship/equipment_confirm_delete.html"
    model = Equipment

    def get_queryset(self):
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        self.space = Space.objects.get(pk=self.kwargs['pk_1'])
        return Equipment.objects.filter(space=self.space)
    
    def get_context_data(self, **kwargs):
        context = super(EquipmentDeleteView, self).get_context_data(**kwargs)
        context['main_slug'] = self.kwargs.get('ship_main_slug')
        context['ship'] = self.ship
        context['assess_name'] = self.assess_name
        context['space'] = self.space
        return context    

    def get_success_url(self):
        main_slug = self.kwargs.get('ship_main_slug')      
        self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
        self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
        self.space = Space.objects.get(pk=self.kwargs['pk_1'])
        return reverse("ship:equipment_list", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk_1':self.space.pk})


@login_required	  
def question(request, **kwargs):
	main_slug = kwargs.get('ship_main_slug')
	ship = Ship.objects.get(ship_slug=kwargs.get('ship_slug'))
	assess_name = Assess_name.objects.get(assess_slug=kwargs.get('assess_slug'))
	space = Space.objects.get(pk=kwargs.get('pk_1'))
	equipment = Equipment.objects.get(pk=kwargs.get('pk'))
	queryset_list = Question.objects.filter(equipment=equipment).order_by('question_order')
	paginator = Paginator(queryset_list, 1)
	page = request.GET.get('page')
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
	   # If page is not an integer, deliver first page.
	   queryset = paginator.page(1)
	except EmptyPage:
	   # If page is out of range (e.g. 9999), deliver last page of results.
	   queryset = paginator.page(paginator.num_pages)
	if request.user.is_authenticated():
		form = AssessmentForm(request.POST or None)
		if form.is_valid():
			question_id = form.cleaned_data.get('question_id')
			answer_id = form.cleaned_data.get('answer_id')
			
			
			question_instance = Question.objects.get(id=question_id)
			answer_instance = Answer.objects.get(id=answer_id)              
			ship_instance = ship             
			assess_name_instance = assess_name
			space_instance = space
			equipment_instance = equipment
			
			
			new_assessment = Assessment()
			new_assessment.ship = ship_instance
			new_assessment.user = request.user
			new_assessment.question = question_instance
	
			new_assessment.assess_type = assess_name_instance
			new_assessment.space = space_instance
			new_assessment.equipment = equipment_instance
			new_assessment.answer = answer_instance
			new_assessment.save()
		else:
			print form.errors
		today = date.today()
		hitform = DiscrepancyForm(request.POST or None,
			initial={'ships': ship,
					 'space': space,
					 'system': equipment,
					 'discover_date':today })	
	
		instance = Question.objects.filter(equipment=equipment).order_by('question_order')
		context = {
			"ship": ship,
			"form": form,
			"hitform": hitform,
			"instance": instance,
			"object_list": queryset_list,
			"queryset": queryset,
			"assess_name": assess_name,
			"main_slug": main_slug
		}
		return render(request, "ship/questions.html", context)
	else:
		raise Http404

class QuestionListView(ListView):
	model = Question
	template_name = "ship/question_list.html"
	def get_queryset(self):
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
		self.space = Space.objects.get(pk=self.kwargs['pk_1'])
		self.equipment = Equipment.objects.get(pk=self.kwargs['pk_2'])
		return Question.objects.filter(equipment=self.equipment)
	
	def get_context_data(self, **kwargs):
		context = super(QuestionListView, self).get_context_data(**kwargs)
		context['main_slug'] = self.kwargs.get('ship_main_slug')
		context['ship'] = self.ship
		context['assess_name'] = self.assess_name
		context['space'] = self.space
		context['equipment'] = self.equipment
		return context


class QuestionCreateView(PermissionRequiredMixin, CreateView):
	permission_required = 'ship.add_question'
	raise_exception=True	
	model = Question
	template_name = "ship/question_form.html"
	form_class = QuestionForm
	def get_queryset(self):
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
		self.space = Space.objects.get(pk=self.kwargs['pk_1'])
		self.equipment = Equipment.objects.get(pk=self.kwargs['pk_2'])
		return Question.objects.filter(equipment=self.equipment)
	
	def get_context_data(self, **kwargs):
		context = super(QuestionCreateView, self).get_context_data(**kwargs)
		context['main_slug'] = self.kwargs.get('ship_main_slug')
		context['ship'] = self.ship
		context['assess_name'] = self.assess_name
		context['space'] = self.space
		context['equipment'] = self.equipment
		return context
	
	def get_initial(self):
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
		self.space = Space.objects.get(pk=self.kwargs['pk_1'])
		self.equipment = Equipment.objects.get(pk=self.kwargs['pk_2'])
		object_count = Question.objects.filter(equipment=self.equipment).count() + 1
		return {'assess_type': self.assess_name, 'space':self.space, 'equipment':self.equipment, 'question_order':object_count }

	def form_valid(self, form):
		form.instance.assess_type = self.assess_name
		form.instance.space = self.space
		form.instance.equipment = self.equipment
		return super(QuestionCreateView, self).form_valid(form)
	
	def get_success_url(self):
		main_slug = self.kwargs.get('ship_main_slug')      
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
		self.space = Space.objects.get(pk=self.kwargs['pk_1'])
		self.equipment = Equipment.objects.get(pk=self.kwargs['pk_2'])
		return reverse("ship:question_create", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk_1':self.space.pk, 'pk_2':self.equipment.pk }) 	

class QuestionUpdateView(PermissionRequiredMixin, UpdateView):
	permission_required = 'ship.change_question'
	raise_exception=True	
	model = Question
	template_name = "form.html"
	form_class = QuestionForm
	def get_queryset(self):
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
		self.space = Space.objects.get(pk=self.kwargs['pk_1'])
		self.equipment = Equipment.objects.get(pk=self.kwargs['pk_2'])
		return Question.objects.filter(equipment=self.equipment)
	
	def get_context_data(self, **kwargs):
		context = super(QuestionUpdateView, self).get_context_data(**kwargs)
		context['main_slug'] = self.kwargs.get('ship_main_slug')
		context['ship'] = self.ship
		context['assess_name'] = self.assess_name
		context['space'] = self.space
		context['equipment'] = self.equipment
		return context

	def get_success_url(self):
		main_slug = self.kwargs.get('ship_main_slug') 
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
		self.space = Space.objects.get(pk=self.kwargs['pk_1'])
		self.equipment = Equipment.objects.get(pk=self.kwargs['pk_2'])
		return reverse("ship:question_create", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk_1':self.space.pk, 'pk_2':self.equipment.pk })

class QuestionDeleteView(PermissionRequiredMixin, DeleteView):
	permission_required = 'ship.delete_question'
	raise_exception=True	
	model = Question
	def get_queryset(self):
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
		self.space = Space.objects.get(pk=self.kwargs['pk_1'])
		self.equipment = Equipment.objects.get(pk=self.kwargs['pk_2'])
		return Question.objects.filter(equipment=self.equipment)

	def get_context_data(self, **kwargs):
		context = super(QuestionDeleteView, self).get_context_data(**kwargs)
		context['main_slug'] = self.kwargs.get('ship_main_slug')
		context['ship'] = self.ship
		context['assess_name'] = self.assess_name
		context['space'] = self.space
		context['equipment'] = self.equipment
		return context

	def get_success_url(self):
		main_slug = self.kwargs.get('ship_main_slug') 
		self.ship = Ship.objects.get(ship_slug=self.kwargs['ship_slug'])
		self.assess_name = Assess_name.objects.get(assess_slug=self.kwargs['assess_slug'])
		self.space = Space.objects.get(pk=self.kwargs['pk_1'])
		self.equipment = Equipment.objects.get(pk=self.kwargs['pk_2'])
		return reverse("ship:question_list", kwargs={'assess_slug':self.assess_name.assess_slug, 'ship_slug':self.ship.ship_slug, 'ship_main_slug':main_slug, 'pk_1':self.space.pk, 'pk_2':self.equipment.pk })

@login_required	
def discrepancy_modal(request, **kwargs):
	hitform = DiscrepancyForm(request.POST or None)
	today = date.today()
	if request.method == "POST":
		if hitform.is_valid():
			ships = hitform.cleaned_data['ships']
			system = hitform.cleaned_data['system']
			description = hitform.cleaned_data['description']			
			department = hitform.cleaned_data['department']
			repair_schedule = hitform.cleaned_data['repair_schedule']
			sat = hitform.cleaned_data['sat']
			sto = hitform.cleaned_data['sto']
			jsn = hitform.cleaned_data['jsn']
			
			new_discrepancy = Discrepancy()
			new_discrepancy.user = request.user
			new_discrepancy.ships = ships
			new_discrepancy.system = system
			new_discrepancy.department = department
	
			new_discrepancy.description = description
			new_discrepancy.discover_date = today
			new_discrepancy.repair_schedule = repair_schedule
			new_discrepancy.sat = sat
			new_discrepancy.sto = sto
			new_discrepancy.jsn = jsn
			new_discrepancy.save()
			return HttpResponse('Success')
		if hitform.errors:
			print hitform.errors
			json_data = json.dumps(hitform.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"hitform": hitform,
	}
	return render(request, "form.html", context)