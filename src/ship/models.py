from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.utils.text import slugify

from .validators import validate_file_extension

from command.models import Unit
from crew.models import Crew
from app_manager.models import App_polymanager

class ship_main(App_polymanager):
	name = models.CharField(max_length=20)
	ship_main_slug = models.SlugField(editable=False, null=True, blank=True)
	def __unicode__(self):
		return (self.name)
	
	def get_absolute_url(self):
			return reverse("ship:ship_main", kwargs={"ship_main_slug": self.ship_main_slug})

def pre_save_ship_main(sender, instance, *args, **kwargs):
	ship_main_slug = slugify(instance.name)
	instance.ship_main_slug = ship_main_slug

pre_save.connect(pre_save_ship_main, sender=ship_main)	

class Ship_class(models.Model):
	name = models.CharField(max_length=50)	
	def __unicode__(self):
		return (self.name)
	
class Class_variant(models.Model):
	variant_name = models.CharField(max_length=50)
	ship_class = models.ForeignKey(Ship_class)

	def __unicode__(self):
		return (self.variant_name)

class Department(models.Model):
	title = models.CharField(max_length=20)
	def __unicode__(self):
		return (self.title)

def upload_location(instance, filename):
	return "%s/%s" %(instance.id, filename)


class Ship(models.Model): 
	ship_name = models.CharField(max_length=20, null=True)
	ship_ID = models.CharField(max_length=3, null=True)
	ship_class = models.ForeignKey(Ship_class)
	class_variant = models.ForeignKey(Class_variant, blank=True, null=True)
	hull_number = models.PositiveIntegerField(null=True)
	image = models.ImageField(upload_to=upload_location,
							null=True, blank=True,
							width_field="width_field",
							height_field="height_field")
	height_field = models.IntegerField(default=0)
	width_field = models.IntegerField(default=0)
	ship_slug = models.SlugField(editable=False)
	crew = models.ForeignKey(Crew, null=True, blank=True)
	uic = models.ForeignKey(Unit, null=True, blank=True)
	
	
	def __unicode__(self):
		return self.ship_name
	
	def get_absolute_url(self):
	        return reverse("ship:ship_detail", kwargs={"ship_slug": self.ship_slug})


def pre_save_ship(sender, instance, *args, **kwargs):
	ship_slug = slugify(instance.ship_ID)
	instance.ship_slug = ship_slug
		
pre_save.connect(pre_save_ship, sender=Ship)


class System(models.Model):
	name = models.CharField(max_length=60)
	ship_class = models.ForeignKey(Ship_class, related_name='class_type', on_delete=models.CASCADE)
	variant = models.ManyToManyField(Class_variant, related_name='variant_type', blank=True)
	ship = models.ManyToManyField(Ship, related_name='single_ship')
	system_slug = models.SlugField(editable=False)
	
	def __unicode__(self):
			return self.name

def pre_save_system(sender, instance, *args, **kwargs):
	system_slug = slugify(instance.name)
	instance.system_slug = system_slug

pre_save.connect(pre_save_system, sender=System)


##Certifications
class Gen_Cert(models.Model):
	certname = models.CharField(max_length=40)
	ships = models.ManyToManyField(Ship, related_name='ships')
	# department = models.ForeignKey(Department)
	instruction = models.CharField(max_length=200, null=True, blank=True)
	perodicity = models.CharField(max_length=20, null=True, blank=True)
	 
	def __unicode__(self):
			return (self.certname)
	
class Local_Cert(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)     
	ship = models.ForeignKey(Ship, on_delete=models.CASCADE)
	name = models.ForeignKey(Gen_Cert, on_delete=models.CASCADE)
	cert_date = models.DateField(null=True)
	expiration_date = models.DateField(null=True)
	next_scheduled = models.CharField(max_length=20, null=True, blank=True)
	poc = models.CharField(max_length=20, null=True, blank=True)
	document = models.FileField(upload_to=upload_location, validators=[validate_file_extension], null=True, blank=True)
	pre_com = models.BooleanField(default=False)
	
	def __unicode__(self):
		return self.name.certname

			


class DiscrepancyManager(models.Manager):
	def active(self, *args, **kwargs):
		return super(DiscrepancyManager, self).filter(active=True)
	
	def inactive(self, *args, **kwargs):
		return super(DiscrepancyManager, self).filter(active=False)
	
class Discrepancy(models.Model):
	CAT_CHOICES = (
		("NONE", "NONE"),
		("CAT2", "CAT2"),
		("CAT3", "CAT3"),
		("CAT4", "CAT4"),
	)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
	ships = models.ForeignKey(Ship, related_name='shiphit', on_delete=models.CASCADE)
	system = models.CharField(max_length=255)
	department = models.ForeignKey(Department, related_name='hitdepartment', on_delete=models.CASCADE)
	description = models.TextField(max_length=None)
	discover_date = models.DateField(null=True)
	repair_schedule = models.CharField(max_length=20, null=True, blank=True)
	sat = models.BooleanField(default=False)
	sto = models.BooleanField(default=False)
	jsn = models.CharField(max_length=20, null=True, blank=True)
	casrep = models.CharField(max_length=4, choices=CAT_CHOICES, default= "NONE", null=True, blank=True)
	image = models.ImageField(upload_to=upload_location,
							null=True, blank=True,
							width_field="width_field",
							height_field="height_field")
	height_field = models.IntegerField(default=0, null=True, blank=True)
	width_field = models.IntegerField(default=0, null=True, blank=True)     
	active = models.BooleanField(default=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)     
	
	objects = DiscrepancyManager()
	
	def __unicode__(self):
			return self.system
		
	def casrep_verbose(self):
		return dict(Discrepancy.CAT_CHOICES)[self.casrep]

class Discrepancy_image(models.Model):
	discrepancy = models.ForeignKey(Discrepancy, on_delete=models.CASCADE)
	image = models.ImageField(upload_to=upload_location,
							null=True, blank=True,
							width_field="width_field",
							height_field="height_field")
	height_field = models.IntegerField(default=0, null=True, blank=True)
	width_field = models.IntegerField(default=0, null=True, blank=True)
	
	def __unicode__(self):
		return self.discrepancy.system

class Assess_name(models.Model):
	name = models.CharField(max_length=60)
	assess_slug = models.SlugField(editable=False)
	
	def __unicode__(self):
			return self.name

	def get_absolute_url(self):
			return reverse("assess_name_list", kwargs={"assess_slug": self.assess_slug})

def pre_save_assess(sender, instance, *args, **kwargs):
	assess_slug = slugify(instance.name)
	instance.assess_slug = assess_slug

pre_save.connect(pre_save_assess, sender=Assess_name)

class Space(models.Model):
	name = models.CharField(max_length=60)
	ship = models.ManyToManyField(Ship, related_name='spaceship')
	assess_name = models.ManyToManyField(Assess_name, related_name='space_assess')
	
	def __unicode__(self):
		return self.name
		
	def get_absolute_url(self):
		return reverse("space_list", kwargs={"space_slug": self.space_slug})


class Equipment(models.Model):
	space = models.ForeignKey(Space, on_delete=models.CASCADE)
	name = models.CharField(max_length=200, null=True, blank=True)
	ship = models.ManyToManyField(Ship, related_name='equipship')    
	assess_name = models.ManyToManyField(Assess_name, related_name='equip_assess')

	def __unicode__(self):
		return self.name

	def get_absolute_url(self):
		return reverse("equipment_list", kwargs={"pk": self.pk})
	
	
class Question(models.Model):
	question = models.TextField()
	assess_type = models.ForeignKey(Assess_name, on_delete=models.CASCADE)
	space = models.ForeignKey(Space, on_delete=models.CASCADE)    
	equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
	answers = models.ManyToManyField('Answer')
	question_order = models.IntegerField(null=True, blank=True)
	
	def __unicode__(self):
		return self.question
		
class Answer(models.Model):
		answer = models.CharField(max_length=120)
		
		def __unicode__(self):
			return self.answer
	
class Assessment(models.Model):
		ship = models.ForeignKey(Ship, on_delete=models.CASCADE)
		user = models.ForeignKey(settings.AUTH_USER_MODEL)
		question = models.ForeignKey(Question)
		assess_type = models.ForeignKey(Assess_name, on_delete=models.CASCADE)
		space = models.ForeignKey(Space, on_delete=models.CASCADE)    
		equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
		answer = models.ForeignKey(Answer)
		timestamp = models.DateTimeField(auto_now_add=True)
		
		def __unicode__(self):
			return str(self.question)