from django.contrib import admin

from .models import (
                Ship_class,
                Class_variant,
                Ship,
                Department,
                Gen_Cert,
                Local_Cert,
                Discrepancy,
                Discrepancy_image,
                Assess_name,
                Space,
                Equipment,
                Question,
                Answer,
                Assessment,
                ship_main,
                System,
)

class Ship_classAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "name",     
]
 
class Class_variantAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "variant_name",
        "ship_class",
]

class ShipAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "ship_name",
        "ship_ID",
        "hull_number",
        "uic",
]

class DepartmentAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]
    
class Gen_CertAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]
    
class Local_CertAdmin(admin.ModelAdmin):          
    list_display = [
        "__unicode__",
    
]

class DiscrepancyAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]

class Discrepancy_imageAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]
    
class Assess_nameAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]
    
class SpaceAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]

class EquipmentAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]
    
class QuestionAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]
    
class AnswerAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]    

class AssessmentAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]

class ship_mainAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "ship_main_slug",
    
]

class SystemAdmin(admin.ModelAdmin):
     list_display = [
        "__unicode__",
]           
   
admin.site.register(Ship_class, Ship_classAdmin)
admin.site.register(Class_variant, Class_variantAdmin)
admin.site.register(Ship, ShipAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Gen_Cert, Gen_CertAdmin)
admin.site.register(Local_Cert, Local_CertAdmin)
admin.site.register(Discrepancy, DiscrepancyAdmin)
admin.site.register(Discrepancy_image, Discrepancy_imageAdmin)
admin.site.register(Assess_name, Assess_nameAdmin)
admin.site.register(Space, SpaceAdmin)
admin.site.register(Equipment, EquipmentAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Assessment, AssessmentAdmin)
admin.site.register(ship_main, ship_mainAdmin)
admin.site.register(System, SystemAdmin)