from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.utils.text import slugify


from django.contrib.auth.models import User
from command.models import Unit, Department, Division
from crew.models import Crew
from profiles.models import Profile

from app_manager.models import App_polymanager

class administration_main(App_polymanager):
	name = models.CharField(max_length=40)
	admin_main_slug = models.SlugField(editable=False, null=True, blank=True)
	def __unicode__(self):
		return (self.name)
	
	def get_absolute_url(self):
			return reverse("administration:admin_main", kwargs={"admin_main_slug": self.admin_main_slug})

def pre_save_admin_main(sender, instance, *args, **kwargs):
	admin_main_slug = slugify(instance.name)
	instance.admin_main_slug = admin_main_slug

pre_save.connect(pre_save_admin_main, sender=administration_main)	