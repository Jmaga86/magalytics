from django.contrib import admin

# Register your models here.
from .models import administration_main

class administration_mainAdmin(admin.ModelAdmin):
	list_display = [
		"__unicode__",
		"name",
		"admin_main_slug",
]
	
admin.site.register(administration_main, administration_mainAdmin)