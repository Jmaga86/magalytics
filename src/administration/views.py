import json

from datetime import date, datetime, time
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404, redirect

from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from profiles.forms import ProfileForm, DesignationForm
from profiles.models import Profile, Designation, Designator
from command.models import Unit
from .forms import ProfileTransferForm, ProfileDetachForm, ProfileAdminForm

@login_required
def admin_main(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	profile = Profile.objects.get(user=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	profile_list = []
	query = request.GET.get("q")
	if query:
			profile_list = Profile.objects.filter(
									Q(unit=unit)&
									Q(user__first_name__icontains=query)|
									Q(user__last_name__icontains=query)
									)
	else:
		profile_list = Profile.objects.filter(unit=unit)
	main_slug = kwargs.get('admin_main_slug')
	context = {
		"profile_list": profile_list,
		"main_slug": main_slug,
		"unit": unit,
		}
	return render(request, "administration/admin_main.html", context)


@login_required
def admin_transfer(request, **kwargs):
	Transfer = 'TRANSFER'
	unit = Unit.objects.get(name=Transfer)
	profile_list = []
	query = request.GET.get("q")
	if query:
			profile_list = Profile.objects.filter(
									Q(unit=unit)&
									Q(user__first_name__icontains=query)|
									Q(user__last_name__icontains=query)
									)
	else:
		profile_list = Profile.objects.filter(unit=unit)
	main_slug = kwargs.get('admin_main_slug')
	context = {
		"profile_list": profile_list,
		"main_slug": main_slug,
		}
	return render(request, "administration/admin_transfer.html", context)


@login_required
@permission_required('profiles.change_profile', raise_exception=True)
def transfer_gain(request, pk=None, **kwargs):
	instance = get_object_or_404(Profile, pk=pk)
	main_slug = kwargs.get('admin_main_slug')
	profileform = ProfileTransferForm(request.POST or None, user=request.user, instance=instance,
						)
	if request.method == "POST":

		if profileform.is_valid():	
			instance = profileform.save(commit=False)
			instance.save()
			return HttpResponseRedirect(reverse("administration:admin_transfer", kwargs={"admin_main_slug": main_slug,}))
		else:
			print profileform.errors
	context = {
		"instance": instance,
		"title": 'Gain Member',
		"profileform": profileform,
		"main_slug": main_slug,
	}
	return render(request, "administration/admin_gain.html", context)

@login_required
@permission_required('profiles.change_profile', raise_exception=True)
def transfer_detach(request, pk=None, **kwargs):
	instance = get_object_or_404(Profile, pk=pk)
	main_slug = kwargs.get('admin_main_slug')
	profileform = ProfileDetachForm(request.POST or None, instance=instance,
						)
	if request.method == "POST":

		if profileform.is_valid():	
			instance = profileform.save(commit=False)
			instance.save()
			return HttpResponseRedirect(reverse("administration:admin_main", kwargs={"admin_main_slug": main_slug,}))
		else:
			print profileform.errors
	context = {
		"instance": instance,
		"title": 'Detach Member',
		"profileform": profileform,
		"main_slug": main_slug,
	}
	return render(request, "administration/admin_detach.html", context)

@login_required
@permission_required('profiles.change_profile', raise_exception=True)
def admin_profile(request, pk=None, **kwargs):
	instance = get_object_or_404(Profile, pk=pk)
	main_slug = kwargs.get('admin_main_slug')
	profileform = ProfileAdminForm(request.POST or None, user=request.user, instance=instance)
	if request.method == "POST":

		if profileform.is_valid():	
			instance = profileform.save()
			return HttpResponseRedirect(reverse("administration:admin_profile_main", kwargs={"admin_main_slug": main_slug, "pk": instance.pk,}))
		else:
			print profileform.errors
	context = {
		"instance": instance,
		"profileform": profileform,
		"main_slug": main_slug,
	}
	return render(request, "administration/admin_profile.html", context)

@login_required
@permission_required('profiles.change_profile', raise_exception=True)
def admin_profile_main(request, pk=None, **kwargs):
	profile = get_object_or_404(Profile, pk=pk)
	main_slug = kwargs.get('admin_main_slug')
	context = {
		"profile": profile,
		"main_slug": main_slug,
		}
	return render(request, "administration/admin_profile_main.html", context)

@login_required
@permission_required('profiles.change_designation', raise_exception=True)
def add_designation(request, pk=None, **kwargs):
	profile = get_object_or_404(Profile, pk=pk)
	main_slug = kwargs.get('admin_main_slug')
	query = request.GET.get("q")
	object_list = []
	if query:
		object_list = Designator.objects.filter(
										Q(code__icontains=query)|
										Q(name__icontains=query)|
										Q(description__icontains=query)
										)
	else:
		object_list = Designator.objects.all()
	form = DesignationForm(request.POST or None,
							initial={'user':profile.user })	
	context = {
		"object_list": object_list,
		"profile": profile,
		"main_slug": main_slug,
		"form": form
		}
	return render(request, "administration/admin_add_designation.html", context)

@login_required
@permission_required('profiles.change_designation', raise_exception=True)
def designation_add_modal(request, **kwargs):
	instance = get_object_or_404(User, username=request.user)
	form = DesignationForm(request.POST or None)
	if request.method == "POST":	
		if form.is_valid():
			designatior = form.cleaned_data['designator']
			user = form.cleaned_data['user']
			new_Designation = Designation()
			new_Designation.designator = designatior
			new_Designation.user = user
			new_Designation.save()
			
			return HttpResponse('Success')			
		if form.errors:
			print form.errors
			json_data = json.dumps(form.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"form": form,
	}
	return render(request, "form.html", context)

@login_required
@permission_required('profiles.delete_designation', raise_exception=True)
def designation_delete_modal(request, **kwargs):

	if request.method =="POST":
		team = request.POST.get('team')
		print team
		member = request.POST.get('member')
		instance = TeamMember.objects.filter(
								Q(title__title=team)&
								Q(member__username=member))
		print instance
		instance.delete()
			
		return HttpResponse('Success')			
		if form.errors:
			print form.errors
			json_data = json.dumps(form.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"form": form,
	}
	return render(request, "form.html", context)

