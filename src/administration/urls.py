from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import TemplateView

from .views import (
                    admin_main,
                    admin_transfer,
                    transfer_gain,
                    transfer_detach,
                    admin_profile,
                    admin_profile_main,
                    add_designation,
                    )

urlpatterns = [
            url(r'^(?P<admin_main_slug>[-\w]+)/administration/$', admin_main, name='admin_main'),
            url(r'^(?P<admin_main_slug>[-\w]+)/administration/transfer/$', admin_transfer, name='admin_transfer'),
            url(r'^(?P<admin_main_slug>[-\w]+)/administration/(?P<pk>\d+)/$', admin_profile_main, name='admin_profile_main'),
            url(r'^(?P<admin_main_slug>[-\w]+)/administration/(?P<pk>\d+)/designation/$', add_designation, name='admin_designation'), 
            url(r'^(?P<admin_main_slug>[-\w]+)/administration/(?P<pk>\d+)/edit/$', admin_profile, name='admin_profile'),           
            url(r'^(?P<admin_main_slug>[-\w]+)/administration/transfer/(?P<pk>\d+)/gain/$', transfer_gain, name='transfer_gain'),
            url(r'^(?P<admin_main_slug>[-\w]+)/administration/transfer/(?P<pk>\d+)/detach/$', transfer_detach, name='transfer_detach'),

    ]