from django import forms
from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.forms.widgets import SelectDateWidget
from profiles.models import Profile
from command.models import Unit, Department


class ProfileTransferForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ProfileTransferForm, self).__init__(*args, **kwargs)
        self.fields['unit'].queryset = Unit.objects.filter(name=self.user.profile.unit.name)
        self.fields['unit'].empty_label = None
    class Meta:
        model = Profile
        fields = [
            'unit',
        ]

class ProfileDetachForm(forms.ModelForm):  
    def __init__(self, *args, **kwargs):
        Transfer = 'TRANSFER'
        super(ProfileDetachForm, self).__init__(*args, **kwargs)
        self.fields['unit'].queryset = Unit.objects.filter(name=Transfer)
        self.fields['unit'].empty_label = None
    class Meta:
        model = Profile
        fields = [
            'unit',
        ]
class ProfileAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.unit = Unit.objects.filter(name=self.user.profile.unit.name)
        super(ProfileAdminForm, self).__init__(*args, **kwargs)
        self.fields['department'].queryset = Department.objects.filter(unit=self.unit)
    class Meta:
        model = Profile
        fields = [
            'rank',
            'department',
            'division',
            'phone_number',
            'check_in',
            'prd',
            'force_password_change',
        ]
        widgets = { 'check_in': SelectDateWidget(years=range(2010, 2030)),
                'prd': SelectDateWidget(years=range(2010, 2030)),
                }