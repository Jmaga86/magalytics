from django.contrib import admin
from app_manager.models import App_polymanager, App_manager
# Register your models here.

class App_polymanagerAdmin(admin.ModelAdmin):
    list_display = [
        "field",     
]
    
class App_managerAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "name",     
]

admin.site.register(App_polymanager, App_polymanagerAdmin)
admin.site.register(App_manager, App_managerAdmin)