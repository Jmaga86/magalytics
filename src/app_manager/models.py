from __future__ import unicode_literals

from django.db import models

from django.db import models
from polymorphic.models import PolymorphicModel
from command.models import Unit, Department

class App_polymanager(PolymorphicModel):
    field = models.CharField(max_length =50)

def upload_location(instance, filename):
	return "%s/%s" %(instance.id, filename)

class App_manager(models.Model):
    name = models.CharField(max_length =50)
    unit = models.ForeignKey(Unit)
    department = models.ForeignKey(Department)
    app_local = models.ForeignKey(App_polymanager)
    icon_name = models.CharField(max_length=50, null=True, blank=True)
    image = models.ImageField(upload_to=upload_location,
                            null=True, blank=True,
                            width_field="width_field",
                            height_field="height_field")
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    
    def __unicode__(self):
        return (self.name)