from django.contrib import admin

from .models import Crew, crew_main


class CrewAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "name",

    ]
    

class crew_mainAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
    
]
admin.site.register(Crew, CrewAdmin)
admin.site.register(crew_main, crew_mainAdmin)