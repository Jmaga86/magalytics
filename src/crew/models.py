from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.utils.text import slugify

from app_manager.models import App_polymanager

class crew_main(App_polymanager):
	name = models.CharField(max_length=20)
	crew_main_slug = models.SlugField(editable=False, null=True, blank=True)
	def __unicode__(self):
		return (self.name)
	
	def get_absolute_url(self):
			return reverse("crew_main", kwargs={"crew_main_slug": self.crew_main_slug})

def pre_save_crew_main(sender, instance, *args, **kwargs):
	crew_main_slug = slugify(instance.name)
	instance.crew_main_slug = crew_main_slug

pre_save.connect(pre_save_crew_main, sender=crew_main)

class Crew(models.Model):
    name = models.CharField(max_length=50)
    number = models.IntegerField()
    on_hull = models.BooleanField(default=False)
    crew_slug = models.SlugField(editable=False)

    def __unicode__(self):
        return self.number
    
                
def pre_save_crew(sender, instance, *args, **kwargs):
    crew_slug = slugify(instance.number)
    instance.slug = crew_slug
        
pre_save.connect(pre_save_crew, sender=Crew)
