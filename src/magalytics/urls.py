from django.contrib.auth.decorators import permission_required
from django.contrib.admin.views.decorators import staff_member_required
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import TemplateView

from command.views import (
	contact_create_modal,
)

from ship.views import (
					ShipCreateView,
					Gen_CertCreateView,
					Gen_CertListView,
					Gen_CertUpdateView,
					Assess_nameListView,
					Assess_nameUpdateView,
					Assess_nameCreateView,
					discrepancy_modal,
					issue_create_modal,

					
)

from profiles.views import (profile_create,
							teammember_create_modal,
							teammember_delete_modal
							)

from dashboard.views import (
				snivel_create_modal,
)

from administration.views import(
							designation_add_modal,
							designation_delete_modal
)
urlpatterns = [
	url(r'^ajax/hit_list/$', discrepancy_modal, name='ajax_hit_list'),
	url(r'^ajax/issue_list/$', issue_create_modal, name='ajax_issue_list'),
	url(r'^ajax/team_main/$', teammember_create_modal, name='ajax_team_main'),
	url(r'^ajax/team_delete/$', teammember_delete_modal, name='ajax_team_delete'),
	url(r'^ajax/snivel_create/$', snivel_create_modal, name='ajax_snivel_create'),
	url(r'^ajax/designation_delete/$', designation_delete_modal, name='ajax_designation_delete'),
	url(r'^ajax/designation_add/$', designation_add_modal, name='ajax_designation_add'),
	url(r'^ajax/contact_add/$', contact_create_modal, name='ajax_contact_add'),	
	url(r'', include('allauth.urls')),
	url(r'',  include('dashboard.urls', namespace='dashboard')),
	url(r'',  include('profiles.urls', namespace='profiles')),   	
	url(r'^admin/', admin.site.urls),
	url(r'^settings/$', staff_member_required(TemplateView.as_view(template_name='settings.html')), name='settings'),
	url(r'^settings/ship/$', ShipCreateView.as_view(), name='ship_create'),
	url(r'^settings/create_profile/$',profile_create, name='create_profile' ),
	url(r'^settings/generic_cert/$', Gen_CertListView.as_view(), name='gen_cert'),
	url(r'^settings/generic_cert/(?P<pk>\d+)/$', Gen_CertUpdateView.as_view(), name='gen_cert_update'),    
	url(r'^settings/generic_cert/create/$', Gen_CertCreateView.as_view(), name='gen_cert_create'),
	url(r'^settings/assessments/$', Assess_nameListView.as_view(), name='gen_assess'),
	url(r'^settings/assessments/(?P<pk>\d+)/$', Assess_nameUpdateView.as_view(), name='gen_assess_update'),    
	url(r'^settings/assessments/create/$', Assess_nameCreateView.as_view(), name='gen_assess_create'),
	url(r'', include('command.urls', namespace='command')),
	url(r'', include('ship.urls', namespace='ship')),
	url(r'', include('administration.urls', namespace='administration')),	
	]

if settings.DEBUG:
    urlpatterns+= static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)