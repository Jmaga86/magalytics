from __future__ import unicode_literals

from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import signals
from django.contrib.auth.models import User

from command.models import Unit, Department, Division

# Create your models here.

P_User = settings.AUTH_USER_MODEL

E1 = "SA"
E2 = "SR"
E3 = "SN"
E4 = "PO3"
E5 = "PO2"
E6 = "PO1"
E7 = "CPO"
E8 = "SCPO"
E9 = "MCPO"

W2 = "CWO2"
W3 = "CWO3"
W4 = "CWO4"
W5 = "CWO5"

O1 = "ENS"
O2 = "LTJG"
O3 = "LT"
O4 = "LCDR"
O5 = "CDR"
O6 = "CAPT"
O7 = "RDML"
O8 = "RADM"
O9 = "VADM"
OO = "ADM"

RANK_CHOICES = (
    (E1, "SA"),
    (E2, "SR"),
    (E3, "SN"),
    (E4, "PO3"),
    (E5, "PO2"),
    (E6, "PO1"),
    (E7, "CPO"),
    (E8, "SCPO"),
    (E9, "MCPO"),
    (W2, "CWO2"),
    (W3, "CWO3"),
    (W4, "CWO4"),
    (W5, "CWO5"),
    (O1, "ENS"),
    (O2, "LTJG"),
    (O3, "LT"),
    (O4, "LCDR"),
    (O5, "CDR"),
    (O6, "CAPT"),
    (O7, "RDML"),
    (O8, "RADM"),
    (O9, "VADM"),
    (OO, "ADM"),
)

class Profile(models.Model):
    user = models.OneToOneField(User)
    unit = models.ForeignKey(Unit)
    department = models.ManyToManyField(Department, blank=True)
    division = models.ForeignKey(Division, null=True, blank=True, on_delete=models.SET_NULL)
    rank = models.CharField(max_length=4, choices=RANK_CHOICES, default= "PO2", null=True, blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+9999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=15, validators=[phone_regex], null=True, blank=True)
    check_in = models.DateField(null=True, blank=True)
    prd = models.DateField(null=True, blank=True)
    force_password_change = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.user.username
    
    class Meta:
        ordering = ['rank']
    
    def get_absolute_url(self):
        url = reverse("profile", kwargs={"username": self.username})
        return url


def password_change_signal(sender, instance, **kwargs):
    try:
        user = User.objects.get(username=instance.username)
        if not user.password == instance.password:
          profile = user.profile
          profile.force_password_change = False
          profile.save()
    except User.DoesNotExist:
        pass

signals.pre_save.connect(password_change_signal, sender=User, dispatch_uid='profiles.models')


ROLL_CHOICES = (
    ('Regular', 'Regular'),
    ('Staff', 'Staff'),
    ('TypeDesk', 'TypeDesk'),
)
class UserRole(models.Model):
    user = models.OneToOneField(User)
    role = models.CharField(max_length=120, default='Regular', choices=ROLL_CHOICES)
    
    def __unicode__(self):
        return self.role

class Designator(models.Model):
    code = models.CharField(max_length=50)
    name = models.CharField(max_length=10)
    description = models.CharField(max_length=100)
    
    def __unicode__(self):
        return self.name
    
class Designation(models.Model):    
    designator = models.ForeignKey(Designator)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.user.username

class Team(models.Model):
    title = models.CharField(max_length=60)
    team_lead = models.ForeignKey(User, null=True, blank=True)
    department = models.ForeignKey(Department)
    
    def __unicode__(self):
        return self.title    
    
class TeamMember(models.Model):
    title = models.ForeignKey(Team)
    member = models.ForeignKey(User)
    
    
    def __unicode__(self):
        return self.member.username
    