from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import TemplateView

from .views import (
                    team_main,
                    TeamCreateView,
                    TeamUpdateView,
                    TeamDeleteView,
                    profile_main,
                    ProfileUpdateView,
                    TeamMemberCreateView,
                    )

from dashboard.views import (
                    snivel_master,
)


urlpatterns = [
    url(r'^profile/(?P<pk>\d+)/$', profile_main, name='profile_main'),
    url(r'^profile/(?P<pk>\d+)/update/$', ProfileUpdateView.as_view(), name='profile_update'),
    url(r'^team_manager/$', team_main, name='team_main'),
    url(r'^team_manager/create/$', TeamCreateView, name='team_create'),
    url(r'^team_manager/(?P<pk>\d+)/change/$', TeamMemberCreateView, name='member_change'),     
    url(r'^team_manager/(?P<pk>\d+)/update/$', TeamUpdateView.as_view(), name='team_update'),
    url(r'^team_manager/(?P<pk>\d+)/delete/$', TeamDeleteView.as_view(), name='team_delete'),
    url(r'^team_manager/snivel/(?P<pk>\d+)/$', snivel_master, name='snivel_master'),

]