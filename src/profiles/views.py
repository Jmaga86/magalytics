import json

from datetime import date, datetime, time
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404, redirect

from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView


from .models import Profile, Team, TeamMember
from .forms import TeamForm, ProfileForm, UserForm, TeamMemberForm, Designator

from command.models import Unit, Department, Division


User = get_user_model()

@login_required
def profile_main(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	profile = Profile.objects.get(user=request.user)
	
	context = {
		"profile": profile,

		}
	return render(request, "profiles/profile_main.html", context)

@login_required
def profile_create(request):	
	if request.method == "POST":
		profileform = ProfileForm(request.POST)
		userform = UserForm(request.POST)
		if profileform.is_valid() and userform.is_valid():			
			user = userform.save(commit=False)
			email = userform.cleaned_data['email']
			user.set_password(userform.cleaned_data['password'])
			username = email.split('@')[0]
			user.is_active = userform.cleaned_data['is_active']
			user.username = username
			user.save()
			profile = profileform.save(commit=False)
			profile.user = user
			profile.save()
			return HttpResponseRedirect("/settings")
		else:
			print profileform.errors
			print userform.errors	
	else:
		userform = UserForm()
		profileform = ProfileForm()
	context = {
		"profileform": profileform,
		"userform": userform,
	}
	return render(request, "profiles/profile_form.html", context)
			
class ProfileUpdateView(PermissionRequiredMixin, UpdateView):
	permission_required = 'profiles.change_profile'
	raise_exception=True
	model = Profile
	template_name = "form.html"
	form_class = ProfileForm
	
	def get_success_url(self):		
			self.profile = Profile.objects.get(pk=self.kwargs['pk'])
			return reverse("profiles:profile_main", kwargs={'pk':self.profile.pk})	

@login_required
def team_main(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	profile = Profile.objects.filter(unit=unit)
	team_list = Team.objects.filter(
						Q(team_lead=user)|
						Q(teammember__member=user)  
						  ).distinct()
	
	context = {
		"profile": profile,
		"team_list": team_list,

		}
	return render(request, "profiles/team_main.html", context)


@login_required
@permission_required('profiles.add_team', raise_exception=True)
def TeamCreateView(request, **kwargs):
	instance = get_object_or_404(User, username=request.user)
	user = instance
	form = TeamForm(request.POST or None, user=request.user, instance=instance)
	if request.method == "POST":
		if form.is_valid():	
			instance = Team()
			instance.title = form.cleaned_data.get('title')
			instance.department = form.cleaned_data.get('department')
			instance.team_lead = user
			instance.save()
			return HttpResponseRedirect(reverse("profiles:team_main"))
		else:
			print form.errors
	context = {
		"form": form,
	}
	return render(request, "form.html", context)	


class TeamUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'profiles.change_team'
    raise_exception=True
    model = Team
    template_name = "form.html"
    form_class = TeamForm
    
    def get_success_url(self):
         return reverse("profile:team_main")

class TeamDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'profiles.delete_team'
    raise_exception=True
    model = Team

    def get_success_url(self):
        return reverse("profiles:team_main")

@login_required
@permission_required('profiles.add_teammember', raise_exception=True)
def TeamMemberCreateView(request, **kwargs):
	instance = get_object_or_404(User, username=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	profile_list = Profile.objects.order_by('rank').values('rank').distinct()
	team = Team.objects.get(pk=kwargs.get('pk'))
	designator_list = Designator.objects.all()
	department_list = Department.objects.filter(unit=unit)
	command_list = []
	query = request.GET.get("q")
	rate = request.GET.get("rate", "")
	rank = request.GET.get("rank", "")
	division = request.GET.get("division", "")
	department = request.GET.get("department", "")
	if rate or rank or department:
			command_list = Profile.objects.filter(
							Q(unit=unit)&
							~Q(user__teammember__title=team)&
							Q(user__designation__designator__name__icontains=rate)&
							Q(rank__icontains=rank)&
							Q(department__name__icontains=department)
							).distinct()
	elif query:
			command_list = Profile.objects.filter(
					Q(unit=instance.profile.unit)&
					~Q(user__teammember__title=team)&
					Q(user__first_name__icontains=query)|
					Q(user__last_name__icontains=query)
					)
	else:
		command_list = Profile.objects.filter(
								Q(unit=instance.profile.unit)&
								~Q(user__teammember__title=team))
	member_list = []
	member_list = Profile.objects.filter(
							Q(unit=instance.profile.unit)&
							Q(user__teammember__title=team))
	form = TeamMemberForm(request.POST or None,
						  initial={'title':team, })
	if request.method == "POST":
		if form.is_valid():
				instance = form.save()
		return HttpResponseRedirect(reverse("profiles:team_main"))
	context = {
		"profile_list": profile_list,
		"designator_list": designator_list,
		"department_list": department_list,
		"instance": instance,
		"team": team,
		"form": form,
		"member_list": member_list,
		"command_list": command_list
	}
	return render(request, "profiles/team_add.html", context)

@login_required
@permission_required('profiles.add_teammember', raise_exception=True)
def teammember_create_modal(request, **kwargs):
	instance = get_object_or_404(User, username=request.user)
	form = TeamMemberForm(request.POST or None)
	if request.method == "POST":	
		if form.is_valid():
			title = form.cleaned_data['title']
			member = form.cleaned_data['member']
			
			new_TeamMember = TeamMember()
			new_TeamMember.title = title
			new_TeamMember.member = member
			new_TeamMember.save()
			
			return HttpResponse('Success')			
		if form.errors:
			print form.errors
			json_data = json.dumps(form.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"form": form,
	}
	return render(request, "form.html", context)

@login_required
@permission_required('profiles.add_teammember', raise_exception=True)
def teammember_delete_modal(request, **kwargs):

	if request.method =="POST":
		team = request.POST.get('team')
		print team
		member = request.POST.get('member')
		instance = TeamMember.objects.filter(
								Q(title__title=team)&
								Q(member__username=member))
		print instance
		instance.delete()
			
		return HttpResponse('Success')			
		if form.errors:
			print form.errors
			json_data = json.dumps(form.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"form": form,
	}
	return render(request, "form.html", context)
