from django.contrib import admin

from .models import Profile, Team, TeamMember, UserRole, Designator, Designation
class ProfileAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "user",
        "unit",
        "division",
]
    
class TeamAdmin(admin.ModelAdmin):
        list_display = [
        "__unicode__",
        "title",

]

class TeamMemberAdmin(admin.ModelAdmin):
        list_display = [
        "__unicode__",

]

class UserRoleAdmin(admin.ModelAdmin):
    list_display =[
        "__unicode__",
        "user",
    ]

class DesignatorAdmin(admin.ModelAdmin):
    list_display =[
        "__unicode__",
        "name",
    ]

class DesignationAdmin(admin.ModelAdmin):
    list_display =[
        "__unicode__",
        "designator",
    ]
    
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Team, TeamMemberAdmin)
admin.site.register(TeamMember, TeamAdmin)
admin.site.register(UserRole, UserRoleAdmin)
admin.site.register(Designator, DesignatorAdmin)
admin.site.register(Designation, DesignationAdmin)