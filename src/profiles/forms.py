from django import forms
from django.contrib.auth.models import User
from django.db import models
from django.forms.widgets import SelectDateWidget
from django.conf import settings


from command.models import Unit, Department
from .models import (
				Profile,
				Team,
				TeamMember,
				Designator,
				Designation,
				)

class ProfileForm(forms.ModelForm):
	class Meta:
		exclude = ('user',)
		model = Profile
		fields = [
			'rank',
			'unit',
			'department',
			'division',
			'phone_number',
			'check_in',
			'prd',
			'force_password_change',
		]
		widgets = { 'check_in': SelectDateWidget(years=range(2010, 2030)),
				   'prd': SelectDateWidget(years=range(2010, 2030)),
				   }

 
class UserForm(forms.ModelForm):
	is_active = forms.BooleanField(required=False)
	password = forms.CharField(widget=forms.PasswordInput)
	password2 = forms.CharField(widget=forms.PasswordInput)
	class Meta:
		model = User
		fields = [
			'email',
			'first_name',
			'last_name',
		]
		widgets = { 'password': forms.PasswordInput(),
				   'password2': forms.PasswordInput(),
				   }		
	def clean(self):
		password = self.cleaned_data.get('password')
		password2 = self.cleaned_data.get('password2')
	
		if password and password != password2:
			raise forms.ValidationError("Passwords don't match")

		return self.cleaned_data
		
	def clean_email(self):
		data = self.cleaned_data['email']
		domain = data.split('@')[1]
		domain_list = ["navy.mil",]
		if domain not in domain_list:
			raise forms.ValidationError("Please enter an Email Address with a valid domain")
		return data

class TeamForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop('user', None)
		self.unit = Unit.objects.filter(profile=self.user.profile)
		super(TeamForm, self).__init__(*args, **kwargs)
		self.fields['department'].queryset = Department.objects.filter(unit=self.unit)  
	class Meta:
		model = Team
		fields = [
			'title',
			'department',
		]      

class TeamMemberForm(forms.ModelForm):
	# def __init__(self, *args, **kwargs):
	# 	self.user = kwargs.pop('user', None)
	# 	super(TeamMemberForm, self).__init__(*args, **kwargs)
	# 	self.fields['member'].queryset = User.objects.filter(profile__unit=self.user.profile.unit)	
	# 	self.fields['title'].empty_label=None
	class Meta:
		model = TeamMember
		fields = [
			'title',
			'member',

		]  


class DesignatorForm(forms.ModelForm):
	class Meta:
		model = Designator
		fields = [
			'code',
			'name',
			'description',

		]
		
class DesignationForm(forms.ModelForm):
	class Meta:
		model = Designation
		fields = [
			'designator',
			'user',

		]

		