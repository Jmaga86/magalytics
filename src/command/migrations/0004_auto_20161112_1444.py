# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-11-12 22:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('command', '0003_department_asset_type_manager'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='unit',
            options={'ordering': ['uic']},
        ),
    ]
