import json
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Q
from django.views.generic import View
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseBadRequest
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Unit, Department, Division, Contact, Instruction
from .forms import UnitForm, DeptForm, DivForm, UnitAssetForm, ContactForm, InstructionForm

User = get_user_model()

#Unit Views 
class UnitCreateView(PermissionRequiredMixin, CreateView):
     permission_required = 'command.add_unit'
     raise_exception=True
     model = Unit
     template_name = "form.html"
     form_class = UnitForm

     def get_success_url(self):
          return ("/settings")

class UnitUpdateView(PermissionRequiredMixin, UpdateView):
     permission_required = 'command.change_unit'
     raise_exception=True
     model = Unit
     template_name = "form.html"
     form_class = UnitForm
     
     def get_object(self):
        return Unit.objects.get(unit_slug=self.kwargs.get('unit_slug'))

     def get_success_url(self):
          return ("/settings")   

class UnitAssetUpdateView(PermissionRequiredMixin, UpdateView):
     permission_required = 'command.change_unit'
     raise_exception=True
     model = Unit
     template_name = "command/asset_update.html"
     form_class = UnitAssetForm
     
     def get_object(self):
        return Unit.objects.get(unit_slug=self.kwargs.get('unit_slug'))

     def get_success_url(self):
          self.unit = Unit.objects.get(unit_slug=self.kwargs['unit_slug'])
          return reverse("command:dept_list", kwargs={'unit_slug':self.unit.unit_slug}) 

class UnitDetailView(DetailView):    
     model = Unit
     context_object_name = 'unit'
     slug_field = 'unit_slug'
     
     def get_object(self):
          return Unit.objects.get(unit_slug=self.kwargs.get('unit_slug'))
     
     def get_context_data(self, **kwargs):
          self.unit = Unit.objects.get(unit_slug=self.kwargs.get('unit_slug'))
          context = super(UnitDetailView, self).get_context_data(**kwargs)
          context['departments'] = Department.objects.filter(unit=self.unit)
          return context
     
class UnitListView(ListView):
     model = Unit
     
     def get_queryset(self):
          self.unit = Unit.objects.all()
          return self.unit.order_by('uic')


#Department Views

class DeptCreateView(PermissionRequiredMixin, CreateView):
     permission_required = 'command.add_department'
     raise_exception=True
     model = Department
     template_name = "form.html"
     form_class = DeptForm
     def get_queryset(self):
          self.unit = Unit.objects.get(unit_slug=self.kwargs['unit_slug'])
          return Department.objects.filter(unit=self.unit) 
     
     def get_success_url(self):
          self.unit = Unit.objects.get(unit_slug=self.kwargs['unit_slug'])
          return reverse("command:dept_list", kwargs={'unit_slug':self.unit.unit_slug})

class DeptUpdateView(PermissionRequiredMixin, UpdateView):
     permission_required = 'command.change_department'
     raise_exception=True
     model = Department
     template_name = "form.html"
     form_class = DeptForm

     
     # def get_success_url(self):
     #      return reverse("ship_list")
     
class DeptDetailView(DetailView):    
     model = Department
     context_object_name = 'dept'
     slug_field = 'dept_slug'
     
     def get_object(self):
          return Department.objects.get(dept_slug=self.kwargs.get('dept_slug'))
     
class DeptListView(ListView):
     model = Department          
     def get_queryset(self):
          self.unit = Unit.objects.get(unit_slug=self.kwargs['unit_slug'])
          return Department.objects.filter(unit=self.unit)

     def get_context_data(self, **kwargs):
         context = super(DeptListView, self).get_context_data(unit=self.unit, **kwargs)
         context['unit'] = Unit.objects.get(unit_slug=self.kwargs['unit_slug'])
         return context     

#Division Views

class DivCreateView(PermissionRequiredMixin, CreateView):
     permission_required = 'command.add_division'
     raise_exception=True
     model = Division
     template_name = "form.html"
     form_class = DivForm
     def get_queryset(self):
          self.unit = Unit.objects.get(unit_slug=self.kwargs['unit_slug'])
          return Department.objects.filter(unit=self.unit)

     def get_context_data(self, **kwargs):
         context = super(DeptListView, self).get_context_data(unit=self.unit, **kwargs)
         context['unit'] = Unit.objects.get(unit_slug=self.kwargs['unit_slug'])
         return context  
     
     
     def get_success_url(self):
          return ("/settings")

class DivUpdateView(PermissionRequiredMixin, UpdateView):
     permission_required = 'command.change_division'
     raise_exception=True
     model = Unit
     template_name = "form.html"
     form_class = DivForm

     
     # def get_success_url(self):
     #      return reverse("ship_list")
     
class DivDetail(DetailView):    
     model = Division
     
class DivListView(ListView):
     model = Division
     def get_queryset(self):
          self.unit = Unit.objects.get(unit_slug=self.kwargs['unit_slug'])
          self.department = Department.objects.get(dept_slug=self.kwargs['dept_slug'])
          return Division.objects.filter(department=self.department)
     def get_context_data(self, **kwargs):
          return super(DivListView, self).get_context_data(department=self.department, unit=self.unit, **kwargs)

@login_required
def contact_list(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	contact_list = []
	query = request.GET.get("q")
	if query:
		contact_list = Contact.objects.filter(
								Q(unit=unit)&
								Q(name__icontains=query)
								)
	else:
		contact_list = Contact.objects.filter(unit=unit)


	form = ContactForm(request.POST or None,
								initial={'unit':unit})
	
	context = {
		"contact_list": contact_list,
		"unit":unit,
		"form": form,
		}
	return render(request, "command/contact_list.html", context)

@login_required
@permission_required('command.add_contact', raise_exception=True)
def contact_create_modal(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	form = ContactForm(request.POST or None)
	if request.method == "POST":	
		if form.is_valid():
			unit = form.cleaned_data['unit']
			phone_number = form.cleaned_data['phone_number']
			name = form.cleaned_data['name']
			instance = Contact()
			instance.unit = unit
			instance.phone_number = phone_number
			instance.name = name
			instance.save()
			
			return HttpResponse('Success')			
		if form.errors:
			print form.errors
			json_data = json.dumps(form.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"form": form,
	}
	return render(request, "form.html", context)

class ContactUpdateView(PermissionRequiredMixin, UpdateView):
	permission_required = 'command.change_contact'
	raise_exception=True
	model = Contact
	form_class = ContactForm
	template_name = "command/contact_form.html"
	
	def get_success_url(self):     
		return reverse("command:contact_list")

class ContactDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'command.delete_contact'
    raise_exception=True
    model = Contact

    def get_success_url(self):
        return reverse("command:contact_list")

@login_required
def Instruction_list(request, **kwargs):
	user = get_object_or_404(User, username=request.user)
	unit = Unit.objects.get(name=request.user.profile.unit.name)
	instruction_list = []
	query = request.GET.get("q")
	if query:
		instruction_list = Instruction.objects.filter(
								Q(unit=unit)&
								Q(name__icontains=query)
								)
	else:
		instruction_list = Instruction.objects.filter(unit=unit)

	context = {
		"instruction_list": instruction_list,
		"unit":unit,
		}
	return render(request, "command/instruction_list.html", context)

class InstructionCreateView(PermissionRequiredMixin, CreateView):
	permission_required = 'command.add_instruction'
	raise_exception=True
	model = Instruction
	template_name = "command/instruction_form.html"
	form_class = InstructionForm
	
	def upload_file(request):
		if request.method == 'POST':
			form = Local_CertForm(request.POST or None, request.FILES or None)
			if form.is_valid():
				handle_uploaded_file(request.FILES['file'])
		else:
			form = UploadFileForm()

	def get_initial(self):
		user = self.request.user
		unit = Unit.objects.get(name=user.profile.unit.name)
		return { 'user': user, 'unit': unit}

	def get_success_url(self):    
		return reverse("command:instruction_list")

class InstructionUpdateView(PermissionRequiredMixin, UpdateView):
	permission_required = 'command.change_instruction'
	raise_exception=True
	model = Instruction
	form_class = InstructionForm
	template_name = "command/instruction_form.html"
	
	def form_valid(self, form):
		obj = form.save(commit=False)
		obj.user = self.request.user
		return super(InstructionUpdateView, self).form_valid(form)	
	
	def get_success_url(self):     
		return reverse("command:instruction_list")

class InstructionDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'command.delete_instruction'
    raise_exception=True
    model = Instruction

    def get_success_url(self):
        return reverse("command:instruction_list")     
     