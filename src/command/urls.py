from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from .views import (
					UnitCreateView,
					UnitUpdateView,
					UnitDetailView,
					UnitListView,
					UnitAssetUpdateView,
					DeptCreateView,
					DeptUpdateView,
					DeptDetailView,
					DeptListView,
					DivCreateView,
					DivUpdateView,
					DivDetail,
					DivListView,
					contact_list,
					ContactUpdateView,
					ContactDeleteView,
					Instruction_list,
					InstructionCreateView,
					InstructionUpdateView,
					InstructionDeleteView,
					)


urlpatterns = [
	url(r'^settings/command/(?P<unit_slug>[-\w]+)/(?P<dept_slug>[-\w]+)/(?P<div_slug>[-\w]+)/update/', DivUpdateView.as_view(), name='div_update'),        
	url(r'^settings/command/(?P<unit_slug>[-\w]+)/(?P<dept_slug>[-\w]+)/create/$', DivCreateView.as_view(), name='div_create'),	
	url(r'^settings/command/(?P<unit_slug>[-\w]+)/(?P<dept_slug>[-\w]+)/update/', DeptUpdateView.as_view(), name='dept_update'),
	url(r'^settings/command/(?P<unit_slug>[-\w]+)/create/$', DeptCreateView.as_view(), name='dept_create'),		
	url(r'^settings/command/(?P<unit_slug>[-\w]+)/update/$', UnitUpdateView.as_view(), name='unit_update'),
	url(r'^settings/command/(?P<unit_slug>[-\w]+)/assets/$', UnitAssetUpdateView.as_view(), name='asset_update'),	
	url(r'^settings/command/(?P<unit_slug>[-\w]+)/(?P<dept_slug>[-\w]+)/$', DivListView.as_view(), name='div_list'),                     
	url(r'^settings/command/create/$', UnitCreateView.as_view(), name='unit_create'),
	url(r'^settings/command/(?P<unit_slug>[-\w]+)/$', DeptListView.as_view(), name='dept_list'),
	url(r'^settings/command/$', UnitListView.as_view(), name='unit_list'),
	url(r'^contact/$', contact_list, name='contact_list'),
	url(r'^contact/(?P<pk>\d+)/update/$', ContactUpdateView.as_view(), name='contact_update'),
	url(r'^contact/(?P<pk>\d+)/delete/$', ContactDeleteView.as_view(), name='contact_delete'),
	url(r'^instruction/$', Instruction_list, name='instruction_list'),
	url(r'^instruction/create/$', InstructionCreateView.as_view(), name='instruction_create'),
	url(r'^instruction/(?P<pk>\d+)/update/$', InstructionUpdateView.as_view(), name='instruction_update'),
	url(r'^instruction/(?P<pk>\d+)/delete/$', InstructionDeleteView.as_view(), name='instruction_delete'),	


]

if settings.DEBUG:
	urlpatterns+= static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)