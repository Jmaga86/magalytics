from django import forms
from django.db import models
from .models import Unit, Department, Division, Contact, Instruction


class UnitForm(forms.ModelForm):
    class Meta:
        model = Unit
        fields = [
            'name',
            'uic',
            'assets',
            'asset_type',
        ]        

class UnitAssetForm(forms.ModelForm):
    class Meta:
        model = Unit
        fields = [
            'assets',
        ]  

class DeptForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DeptForm, self).__init__(*args, **kwargs)
        self.fields['unit'].empty_label=None      
    class Meta:
        model = Department
        fields = [
            'name',
            'unit',
        ] 

class DivForm(forms.ModelForm):
    class Meta:
        model = Division
        fields = [
            'name',
            'unit',
            'department',
            ]
        
class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = [
            'name',
            'phone_number',
            'unit',
        ]
        
class InstructionForm(forms.ModelForm):
    class Meta:
        model = Instruction
        fields = [
            'user',
            'name',
            'document',
            'unit',
        ]