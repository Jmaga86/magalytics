from django.contrib import admin

from .models import Unit, Department, Division, Asset_Type, Contact, Instruction

class UnitAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "name",     
]
 
class DepartmentAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "name",
        "unit",
        "dept_slug",
]

class DivisionAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "name",
        "department",
        "unit",
        "div_slug",       
]

class Asset_TypeAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "name",
    ]

class ContactAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "phone_number",
    ]

class InstructionAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "user"
    ]

admin.site.register(Unit, UnitAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Division, DivisionAdmin)
admin.site.register(Asset_Type, Asset_TypeAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Instruction, InstructionAdmin)
