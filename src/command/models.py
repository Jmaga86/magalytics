from __future__ import unicode_literals

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.text import slugify

from ship.validators import validate_file_extension

class Asset_Type(models.Model):
	name = models.CharField(max_length=50)
	
	def __unicode__(self):
		return str(self.name)

class Unit(models.Model):
	name = models.CharField(max_length=50)
	unit_slug = models.SlugField(unique=True)
	uic = models.IntegerField()
	assets = models.ManyToManyField("Unit", blank=True)
	asset_type = models.ManyToManyField(Asset_Type, blank=True)

	
	class Meta:
		ordering = ['uic']

	def __unicode__(self):
		return str(self.name)

def create_unit_slug(instance, new_slug=None):
	unit_slug = slugify(instance.name)
	if new_slug is not None:
		unit_slug = new_slug
	qs = Unit.objects.filter(unit_slug=unit_slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(unit_slug, qs.first().id)
		return create_unit_slug(instance, new_slug=new_slug)
	return unit_slug

def pre_save_receiver(sender, instance, *args, **kwargs):
	if not instance.unit_slug:
		instance.unit_slug = create_unit_slug(instance)

class Department(models.Model):
	name = models.CharField(max_length=50)
	dept_slug = models.SlugField(unique=True)
	unit = models.ForeignKey(Unit, blank=True, null=True)
	asset_type_manager = models.ManyToManyField(Asset_Type, blank=True)
	
	class Meta:
		ordering = ['name']

	def __unicode__(self):
		return str(self.name)

def create_dept_slug(instance, new_slug=None):
	dept_slug = slugify(instance.name)
	if new_slug is not None:
		dept_slug = new_slug
	qs = Department.objects.filter(dept_slug=dept_slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(dept_slug, qs.first().id)
		return create_dept_slug(instance, new_slug=new_slug)
	return dept_slug

def pre_save_dept_receiver(sender, instance, *args, **kwargs):
	if not instance.dept_slug:
		instance.dept_slug = create_dept_slug(instance)

class Division(models.Model):
    name = models.CharField(max_length=50)
    div_slug = models.SlugField(unique=True)
    unit = models.ForeignKey(Unit, blank=True, null=True)
    department = models.ForeignKey(Department, blank=True, null=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return str(self.name)

def create_div_slug(instance, new_slug=None):
	div_slug = slugify(instance.name)
	if new_slug is not None:
		div_slug = new_slug
	qs = Division.objects.filter(div_slug=div_slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(div_slug, qs.first().id)
		return create_div_slug(instance, new_slug=new_slug)
	return div_slug

def pre_save_div_receiver(sender, instance, *args, **kwargs):
	if not instance.div_slug:
		instance.div_slug = create_div_slug(instance)

class Contact(models.Model):
	name = models.CharField(max_length=140)
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+9999999999'. Up to 15 digits allowed.")
	phone_number = models.CharField(max_length=15, validators=[phone_regex])
	unit = models.ForeignKey(Unit)
	
	def __unicode__(self):
		return str(self.name)

def upload_location(instance, filename):
	return "%s/%s" %(instance.id, filename)
	
class Instruction(models.Model):
	name = models.CharField(max_length=255)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
	document = models.FileField(upload_to=upload_location, validators=[validate_file_extension])
	unit = models.ForeignKey(Unit, blank=True, null=True)
	
	def __unicode__(self):
		return str(self.name)

pre_save.connect(pre_save_receiver, sender=Unit)
pre_save.connect(pre_save_dept_receiver, sender=Department)
pre_save.connect(pre_save_div_receiver, sender=Division)